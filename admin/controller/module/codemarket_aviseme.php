<?php

class ControllerModuleCodemarketAviseme extends Controller
{
    private $error = array();

    public function index()
    {
        $this->language->load('module/codemarket_aviseme');

        $this->document->setTitle($this->language->get('heading_title_main'));

        $this->load->model('setting/setting');

        if (version_compare(VERSION, '2.2.0.0', '>=')) {
            $ssl = true;
        } else {
            $ssl = 'SSL';
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('codemarket_aviseme', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], $ssl));
        }

        $data['heading_title'] = $this->language->get('heading_title_modulo');
        $data['text_feito']    = $this->language->get('text_feito');

        $data['entry_status']  = $this->language->get('entry_status');
        $data['button_save']   = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], $ssl),
            'separator' => false,
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], $ssl),
            'separator' => ' :: ',
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/codemarket_aviseme', 'token=' . $this->session->data['token'], $ssl),
            'separator' => ' :: ',
        );

        $data['action'] = $this->url->link('module/codemarket_aviseme', 'token=' . $this->session->data['token'], $ssl);
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], $ssl);

        $this->load->model('module/codemarket_aviseme');
        $url   = '';
        $sort  = 'modificado';
        $order = 'ASC';

        if (isset($this->request->get['sort'])) {
            //$url .= '&sort=' . $this->request->get['sort'];
            $sort = $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        $data['sort']  = $sort;
        $data['order'] = $order;

        $data['sort_nome']       = $this->url->link('module/codemarket_aviseme', 'token=' . $this->session->data['token'] . '&sort=nome' . $url, $ssl);
        $data['sort_situacao']   = $this->url->link('module/codemarket_aviseme', 'token=' . $this->session->data['token'] . '&sort=enviado' . $url, $ssl);
        $data['sort_criado']     = $this->url->link('module/codemarket_aviseme', 'token=' . $this->session->data['token'] . '&sort=criado' . $url, $ssl);
        $data['sort_modificado'] = $this->url->link('module/codemarket_aviseme', 'token=' . $this->session->data['token'] . '&sort=modificado' . $url, $ssl);

        $data['dados']           = $this->model_module_codemarket_aviseme->listar($sort, $order);
        $data['listar_produtos'] = $this->model_module_codemarket_aviseme->listar_produtos();

        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');

        $data['editar'] = $this->url->link('extension/codemarket_module', 'token=' . $this->session->data['token'], $ssl);

        if (version_compare(VERSION, '2.2.0.0', '>=')) {
            $this->response->setOutput($this->load->view('module/codemarket_aviseme', $data));
        } else {
            $this->response->setOutput($this->load->view('module/codemarket_aviseme.tpl', $data));
        }
    }

    public function install()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS code_aviseme (
        id_code_aviseme int NOT NULL AUTO_INCREMENT,
        id_produto int NOT NULL,
        id_opcao varchar(350) DEFAULT NULL,
        email varchar(65) NOT NULL,
        dados text NOT NULL,
        dados_enviado text NOT NULL,
        enviado char(1) DEFAULT NULL,
        criado datetime NOT NULL,
        modificado datetime NOT NULL,
        PRIMARY KEY (id_code_aviseme)
        ) ENGINE=InnoDB
        ");
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/codemarket_aviseme')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}
