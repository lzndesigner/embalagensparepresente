<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-body">
        <?php if (isset($listar_produtos) and !empty($listar_produtos)) { ?>
         <h3>Quantidade por Produto</h3>
         <table class="table table-bordered table-striped table-hover">
         <thead>
              <tr>
                <td class="left">Produto</td>
                <td class="left">Quantidade Pendente</td>
                <td class="left">Quantidade Enviada</td>
                <td class="left">Quantidade Total</td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($listar_produtos as $p) { ?>  
              <tr>
                <td class="left"><a href="<?php echo $link; ?>"><?php echo $p['produto']; ?></a></td>
                <td class="left"><?php echo $p['total_pendente']; ?></td>
                <td class="left"><?php echo $p['total_enviado']; ?></td>
                <td class="left"><?php echo $p['total']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
   <?php } ?>
        <h3>Lista Geral</h3>
        <table class="table table-bordered table-striped table-hover">
         <thead>
              <tr>
                <td class="left">Produto</td>
                <td class="left">Opção</td>
                <td class="left">E-mail do Cliente</td>
                <td class="text-left"><?php if (isset($sort) and $sort == 'enviado') { ?>
                  <a href="<?php echo $sort_situacao; ?>" class="<?php echo strtolower($order); ?>">Situação</a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_situacao; ?>">Situação</a>
                  <?php } ?>
                </td>
                <td class="text-left"><?php if (isset($sort) and $sort == 'criado') { ?>
                  <a href="<?php echo $sort_criado; ?>" class="<?php echo strtolower($order); ?>">Criado</a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_criado; ?>">Criado</a>
                  <?php } ?>
                </td>
                <td class="text-left"><?php if (isset($sort) and $sort == 'modificado') { ?>
                  <a href="<?php echo $sort_modificado; ?>" class="<?php echo strtolower($order); ?>">Modificado</a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_modificado; ?>">Modificado</a>
                  <?php } ?>
                </td>
              </tr>
            </thead>
            <tbody>
              <?php if ($dados) { ?>
              <?php foreach ($dados as $dado) { 
                
                $dados1 = unserialize($dado['dados']);
                $email = $dado['email'];
                if(!empty($dados1['opcao'])) {
                $opcao = $dados1['opcao'];
                }else {
                $opcao = 'Sem Opção';
                }
                $imagem = $dados1['imagem_produto'];
                $link = $dados1['link'];
                $nome_produto = $dados1['nome_produto'];
                $criado =  date('d/m/Y h:i:s A', strtotime($dado['criado']));
                $modificado =  date('d/m/Y h:i:s A', strtotime($dado['modificado']));
                if($dado['enviado'] == 0){
                  $enviado = 'Pendente';
                }else{
                  $enviado = 'Enviado';
                }
                $dados_enviado = $dado['dados_enviado'];
              ?>  
              <tr>
                <td class="left"><a href="<?php echo $link; ?>"><?php echo $nome_produto; ?></a></td>
                <td class="left"><?php echo $opcao; ?></td>
                <!--<td class="left"><?php echo html_entity_decode($dados_enviado, ENT_QUOTES, 'UTF-8'); ?></td>-->
                <td class="left"><?php echo $email; ?></td>
                <td class="left"><?php echo  $enviado; ?></td>
                <td class="left"><?php echo  $criado; ?></td>
                <td class="left"><?php echo  $modificado; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="6">Nenhum E-mail Enviado ainda</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <h4><?php echo $text_feito; ?></h4>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>