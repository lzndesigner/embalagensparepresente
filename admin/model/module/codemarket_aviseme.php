<?php
class ModelModuleCodemarketAviseme extends Model
{
    public function listar($sort, $order)
    {
        $sort  = $sort . ' ' . $order;
        $query = $this->db->query("SELECT * FROM code_aviseme
        ORDER BY " . $sort . "
        ");

        return $query->rows;
    }

    public function listar_produtos()
    {
        $query = $this->db->query("SELECT id_produto, dados FROM code_aviseme
        GROUP BY id_produto
        ORDER BY COUNT(*)  DESC
        ");

        $lista = array();

        if (isset($query->rows) and isset($query->rows[0]['id_produto'])) {
            $produtos = $query->rows;
            foreach ($produtos as $p) {

                $query = $this->db->query("SELECT COUNT(id_produto) as total FROM code_aviseme
        WHERE enviado = 0 and id_produto = '" . (int) $p['id_produto'] . "'
        LIMIT 1
        ");
                if (isset($query->row['total'])) {
                    $total_pendente = $query->row['total'];
                } else {
                    $total_pendente = 0;
                }

                $query1 = $this->db->query("SELECT COUNT(id_produto) as total FROM code_aviseme
        WHERE enviado = 1 and id_produto = '" . (int) $p['id_produto'] . "'
        LIMIT 1
        ");

                if (isset($query1->row['total'])) {
                    $total_enviado = $query1->row['total'];
                } else {
                    $total_enviado = 0;
                }

                $total = $total_pendente + $total_enviado;

                $dados1  = unserialize($p['dados']);
                $lista[] = array(
                    'total_pendente' => $total_pendente,
                    'total_enviado'  => $total_enviado,
                    'total'          => $total,
                    'produto'        => $dados1['nome_produto'],
                );
            }
        }

        return $lista;
    }

    public function avisar($id_produto, $id_opcao = '')
    {
        $query = $this->db->query("SELECT * FROM code_aviseme  WHERE
        id_produto =  '" . (int) $id_produto . "'
        AND enviado = 0
        ");

        if (!empty($query->rows)) {
            //$this->fire->log($id_opcao, 'Dentro do Avisar() ID Opcao');
            if (!empty($id_opcao)) {
                //$this->fire->log('Existe Opções');
                foreach ($id_opcao as $o) {
                    //$o['id'] = 18;
                    //$this->fire->log('Dentro do laço das opções');
                    self::enviar($id_produto, $query->rows, $o['id']);
                }
            }
            //$this->fire->log('Rodando sem opção');
            self::enviar($id_produto, $query->rows, 0);
        }
    }

    public function enviar($id_produto, $query, $id_opcao = '')
    {
        $this->load->model('module/codemarket_module');
        $conf = $this->model_module_codemarket_module->getModulo('341');

        if (isset($conf->c5) and $conf->c5 == 1) {
            //$this->fire->log($id_opcao, 'Dentro do Enviar() ID Opção');
            $data = array();
            if ($id_opcao != 0) {
                foreach ($query as $k => $d) {
                    $t  = false;
                    $op = explode(",", $d['id_opcao']);
                    foreach ($op as $ip) {
                        if ($ip == $id_opcao) {
                            $t = true;
                        }
                    }
                    if ($t == false) {
                        unset($query[$k]);
                    }
                }
            } else {
                $query = $this->db->query("SELECT * FROM code_aviseme  WHERE
                id_produto =  '" . (int) $id_produto . "'
                AND id_opcao = 0
                AND enviado = 0
                ");

                $query = $query->rows;
            }

            //$this->fire->log($query->rows, 'Avisos retornados');
            if (!empty($query)) {
                //$this->fire->log('Busca retornada com sucesso');
                foreach ($query as $dado) {
                    //$this->fire->log($dado, 'Enviando aviso');
                    $dados = unserialize($dado['dados']);
                    /*Array (
                    [opcao] => Select: Verde
                    [imagem_produto] => <img src='http://localhost:8080/opencart/image/cache/data/demo/iphone_1-120x120.jpg' title='Produto teste66' alt='Produto teste66' id='image' />
                    [link] => http://localhost:8080/opencart/index.php?route=product/product&product_id=40 [nome_produto] => Produto teste66 )
                     */

                    $email = $dado['email'];

                    $opcao = '';
                    if (isset($dados['opcao']) and !empty($dados['opcao'])) {
                        $opcao = " - " . $dados['opcao'];
                    }

                    $imagem       = $dados['imagem_produto'];
                    $link         = $dados['link'];
                    $nome_produto = $dados['nome_produto'] . $opcao;
                    $assunto      = "O Produto que você queria chegou - " . $nome_produto;

                    $conteudo = "
                    " . $assunto . "<br>
                    " . $imagem . "
                    <b>Produto:</b> <a href='" . $link . "' >" . $nome_produto . "</a><br>
                    <b>Link do Produto:</b> <a href='" . $link . "' >" . $link . "</a><br>
                    <br><b>Aproveite, antes que acabe! <b><br><br>
                    <table border='0' cellpadding='0' cellspacing='0' style='background-color: rgb(236, 118, 50);
                    border: 1px solid #D86420;
                    border-radius: 5px;
                    height: 35px;
                    width: 300px;'>
                    <tr>
                    <td align='center' valign='middle' style='color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:22px; font-weight:bold; letter-spacing:-.5px; line-    height:150%; padding-top:15px; padding-right:30px; padding-bottom:15px; padding-left:30px;'>
                    <a href='" . $link . "' target='_blank' style='color:#FFFFFF; text-decoration:none;'>Ir para o Produto Agora</a>
                    </td>
                    </tr>
                    </table>";

                    $dados_enviado = "
                    <b>Assunto: </b>" . $assunto . "<br><br>
                    <b>Conteúdo: </b>" . $conteudo . "
                    ";

                    $conteudo = html_entity_decode($conteudo, ENT_QUOTES, 'UTF-8');

                    if (version_compare(VERSION, '2.2.0.0', '>=')) {
                        $mail                = new Mail();
                        $mail->protocol      = $this->config->get('config_mail_protocol');
                        $mail->parameter     = $this->config->get('config_mail_parameter');
                        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                        $mail->smtp_port     = $this->config->get('config_mail_smtp_port');
                        $mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout');
                    } elseif (version_compare(VERSION, '2.2.0.0', '<')) {
                        $mail = new Mail($this->config->get('config_mail'));
                    }

                    if (isset($conf->c3) and !empty($conf->c3)) {
                        $mail->setTags($conf->c3);
                    }

                    $mail->setTo($email);
                    $mail->setFrom($this->config->get('config_email'));
                    $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
                    $mail->setSubject(html_entity_decode($assunto, ENT_QUOTES, 'UTF-8'));
                    $mail->setHtml($conteudo);
                    $mail->send();

                    $id = $dado['id_code_aviseme'];
                    $this->db->query("UPDATE code_aviseme SET
                    enviado = '1',
                    dados_enviado = '" . $this->db->escape($dados_enviado) . "',
                    modificado = NOW()
                    WHERE id_code_aviseme = '" . (int) $id . "'
                    ");
                }
            }
        }
    }
}
