<?php
// Heading
$_['heading_title']    = 'Desconto à vista';

// Text
$_['text_total']       = 'Total dos Pedidos';
$_['text_success']     = 'Sucesso: Você modificou a porcentagem de desconto!';
$_['text_edit']        = 'Configurações do Desconto à vista';

// Entry
$_['entry_total']	   = 'Quantos porcento de desconto?';
$_['entry_pedido_total']	= 'Valor do pedido deve alcançar?';
$_['entry_methods']    = 'Dar desconto para (ID):';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Ordem:';

// Help
$_['help_methods'] = '(Ex: "bank_transfer" = Depósito Bancario - sem espaços, sem as aspas!)';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar este módulo!';
?>