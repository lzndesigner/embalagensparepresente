<?php
// Heading
$_['heading_title']    = 'Banner Diversos';

// Text
$_['text_module']      = 'Extensões';
$_['text_success']     = 'Extensões Banner modificado com sucesso!';
$_['text_edit']        = 'Configurações do Extensões Banner';

// Entry
$_['entry_name']       = 'Título';
$_['entry_banner']     = 'Escolha';
$_['entry_dimension']  = 'Dimensão (Largura x Altura)';
$_['entry_width']      = 'Largura';
$_['entry_height']     = 'Altura';
$_['entry_status']     = 'Situação';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o Extensões Banner!';
$_['error_name']       = 'O Título deve ter entre 3 e 64 caracteres!';
$_['error_width']      = 'A largura é obrigatória!';
$_['error_height']     = 'A altura é obrigatória!';