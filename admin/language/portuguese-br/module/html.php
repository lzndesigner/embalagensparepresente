<?php
// Heading
$_['heading_title']     = 'Conteúdo HTML';

// Text
$_['text_module']       = 'Extensões';
$_['text_success']      = 'Exntesão Conteúdo HTML modificado com sucesso!';
$_['text_edit']         = 'Configurações do módulo Conteúdo HTML';

// Entry
$_['entry_name']        = 'Extensão';
$_['entry_title']       = 'Título';
$_['entry_description'] = 'Conteúdo';
$_['entry_status']      = 'Situação';

// Error
$_['error_permission']  = 'Atenção: Você não tem permissão para modificar o Exntesão Conteúdo HTML!';
$_['error_name']        = 'O Extensão deve ter entre 3 e 64 caracteres!';