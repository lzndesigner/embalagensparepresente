<?php echo $header; ?>

<div class="breadcrumb clearfix bgGrey">

  <div class="container">

    <h5><?php echo $heading_title; ?></h5>

    <ul class="">

      <?php foreach ($breadcrumbs as $breadcrumb) { ?>

      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

      <?php } ?>

    </ul>

  </div>

</div><!-- breadcrumb -->


<div class="container">



  <div class="row"><?php echo $column_left; ?>



    <?php if ($column_left && $column_right) { ?>



    <?php $class = 'col-sm-6'; ?>



    <?php } elseif ($column_left || $column_right) { ?>



    <?php $class = 'col-sm-9'; ?>



    <?php } else { ?>



    <?php $class = 'col-sm-12'; ?>



    <?php } ?>

    <!-- BOF Related Options -->
			<div id="content" class="<?php echo $class; ?> product-info"><?php echo $content_top; ?>
			<!-- EOF Related Options -->
      <section class="productDetail">

        <div class="basicDetails">

          <div class="row">

            <div id="product">

              <div class="row">

              <div class="col-xs-12 col-md-5">
                <?php if ($thumb || $images) { ?>

                <style>
                #gallery_01{width: 100%; float:left; clear: both;}
                #gallery_01 img{width:calc(20% - 10px); float:left; margin:5px;}
                </style>
                <script type="text/javascript">
                $(document).ready(function () {
                  $("#zoom_03").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', scrollZoom : true, easing : true, imageCrossfade: false}); 

                      //tr the images to Fancybox
                      $("#zoom_03").bind("click", function(e) {  
                        var ez =   $('#zoom_03').data('elevateZoom'); 
                        $.fancybox(ez.getGalleryList());
                        return false;
                      });
                    }); 
                </script>

                <div class="clearfix">
                  <?php if ($thumb) { ?> 
                  <img id="zoom_03" src="<?php echo $thumb; ?>" data-zoom-image="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"/>
                  <?php }// end if $thumb ?>                 

                </div><!-- clearfix -->

                <?php if ($price) { ?>
                <?php if ($special) { ?>
                <span class="tag"><small><?php echo $percent; ?></small></span>
                <?php } ?>
                <?php }?>

                <div id="gallery_01">

                  <?php if ($thumb) { ?>  
                  <a href="#" data-image="<?php echo $thumb; ?>" data-zoom-image="<?php echo $thumb; ?>">
                    <img id="zoom_03" src="<?php echo $thumb; ?>"  alt="<?php echo $heading_title; ?>" />
                  </a>  
                  <?php } ?>

                  <?php if ($images) { ?>

                  <?php foreach ($images as $image) { ?>
                  <a href="#" data-image="<?php echo $image['thumb']; ?>" data-zoom-image="<?php echo $image['thumb']; ?>">
                    <img id="zoom_03" src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>" />
                  </a>  
                  <?php } ?>

                  <?php } ?>

                </div><!-- gallery_01 -->

                <?php }// end if thumb || images ?> 


                <div class="share-button-group">
                  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5922d7a2effa4eb1"></script>                   
                  <div class="addthis_inline_share_toolbox" style="margin:15px 0;"></div>
                </div><!-- End .share-button-group -->

                
       <?php  if($description == "<p><br></p>") {}else{ ?>
          <?php if ($description) {?>
          <div class="clearfix"></div>
          <hr>
          <div class="boxDescription">
            <?php echo $description; ?>
          </div><!-- tab-pane -->
          <?php } ?>
          <?php } ?>

              </div><!-- col-md-6 -->



              <div class="col-xs-12 col-md-7">
                <h2><?php echo $heading_title; ?></h2>
                <span class="article">
                  <?php if($model){ ?><?php echo $text_model; ?> <b><?php echo $model; ?></b><?php } ?>
                  <?php if ($reward) { ?>| <?php echo $text_reward; ?> <b>
            <span class="<?php echo $live_options['live_options_reward_container']; ?>"><?php echo $reward; ?></span>
            </b><?php } ?>
                  <?php if ($manufacturer) { ?>| <?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><b><?php echo $manufacturer; ?></a></b><?php } ?>
                </span>

                <div class="boxProductPrice">
                  <div class="boxProductPriceLeft">
                       <!-- preco -->  
                      <?php if ($price) { ?>
                        <?php if (!$special) { ?>
                              <span class="price">
            <span class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></span>
            </span>
                        <?php } else { ?> 
                          <span class="price">
                           <span class="priceOld"><strike>
            <span class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></span>
            </strike></span>
                           <br>
                           
            <span class="<?php echo $live_options['live_options_special_container']; ?>"><?php echo $special; ?></span>
            
                           <?php if($saving){ ?><p><i class="fa fa-tags"></i> <?php echo $saving_text; ?> <b><?php echo $saving; ?></b></p><?php } ?>
                         </span>
                        <?php } ?>

                      <?php } ?>
                       <!-- preco -->  
                       <script>
                        $(document).ready(function() {

                           if($("#input-quantity").val() <= 1){
                            $('#menos').addClass('disabled');                        
                            }else{
                            $('#menos').removeClass('disabled');  
                            }

                          $("#mais").click(function(){
                            if($("#input-quantity").val() >= 1){
                            $('#menos').removeClass('disabled');  
                            $('#input-quantity').val(parseInt($('#input-quantity').val())+1);
                            }

                          });// mais

                          $("#menos").click(function(){
                              if($("#input-quantity").val()!=1){                                        
                                $('#input-quantity').val(parseInt($('#input-quantity').val())-1);         
                                if($("#input-quantity").val() <= 1){
                                      $('#menos').addClass('disabled');  
                                  }             
                              }
                          });// menos
                        });
                        </script>


                        <div class="btn-group btn-quantity">
                          <b>Quantidade</b><br>
                          <button type="button" id="menos" class="btn btn-custom-2 btn-sm"><i class="fa fa-minus"></i></button>
                          <input type="text" id="input-quantity" name="quantity" value="1" min="1">
                          <button type="button" id="mais" class="btn btn-custom-2 btn-sm"><i class="fa fa-plus"></i></button>
                        </div>
                  </div><!-- boxProductPriceLeft -->
                  <?php if($shipping_free == 0 || $points || $discounts || !$quantity == '0' || $options){ ?>
                  <div class="boxProductPriceRight">
                        <?php  if ($shipping_free == 0) {
                          echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='Frete Grátis' style='width:42%; float:right; margin:6px;' />";
                        } ?>

                    <?php if ($points) { ?>
                       <span class="price"><?php echo $text_points; ?> 
            <span class="<?php echo $live_options['live_options_points_container']; ?>"><?php echo $points; ?></span>
            </span>
                    <?php } ?>

                     <!-- begin option values -->
                      <?php if(!$quantity == '0'){ ?>
                      <?php if ($options) { ?>
<!-- BOF Related Options -->
			<div class="options">
			<!-- EOF Related Options -->
                      <h4><?php echo $text_option; ?></h4>
                      <?php foreach ($options as $option) { ?>

                      <?php if ($option['type'] == 'select') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <!--BOF Related Options-->
			<option value="<?php echo $option_value['product_option_value_id']; ?>" <?php echo 'master-option-value="' . $option_value['master_option_value'] . '"' . ' option-value="' . $option_value['option_value_id'] . '"'; ?>><?php echo $option_value['name']; ?>
			<!--EOF Related Options-->
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio <?php if($option_value['image']){ ?>radioImg<?php } ?> <?php if($option_value['qta'] <= 0){echo "disabled";} ?>">
                  <!--BOF Related Options--><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><!--EOF Related Options-->
                  <!--BOF Related Options-->
					<input type="radio" <?php if($option_value['qta'] <= 0){echo "disabled";} ?> name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" <?php echo 'master-option-value="' . $option_value['master_option_value'] . '"' . ' option-value="' . $option_value['option_value_id'] . '"'; ?> />
					<!--EOF Related Options-->
                  <?php if($option_value['image']){ ?>
                  <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" width="80" />
                  <?php } ?>

                    <span>
                      <?php echo $option_value['name']; ?>
                    </span>
                  </label>
                  <?php if($option_value['image']){ ?><a href="<?php echo $option_value['image']; ?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a><?php } ?>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <!--BOF Related Options--><div class="checkbox" style="margin-top: -5px;"><!--EOF Related Options-->
                  <!--BOF Related Options--><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><!--EOF Related Options-->
                    <!--BOF Related Options-->
						<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" <?php echo 'master-option-value="' . $option_value['master_option_value'] . '"' . ' option-value="' . $option_value['option_value_id'] . '"'; ?> />
					<!--EOF Related Options-->
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    <!--BOF Related Options--><span>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)</span><!--EOF Related Options-->
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'image') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio radioImg">
                  <!--BOF Related Options--><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><!--EOF Related Options-->
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" width="80" /> <p><?php echo $option_value['name']; ?></p>
                    <?php if ($option_value['price']) { ?>
                    <!--BOF Related Options--><span>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)</span><!--EOF Related Options-->
                    <?php } ?>
                  </label>
                  <a href="<?php echo $option_value['image']; ?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <!--BOF Related Options-->
			  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" <?php echo 'master-option-value="' . $option['master_option_value'] . '"'; ?> />
			  <!--EOF Related Options-->
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <!--BOF Related Options-->
			  <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" <?php echo 'master-option-value="' . $option['master_option_value'] . '"'; ?>placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
			  <!--EOF Related Options-->
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label"><?php echo $option['name']; ?></label>
              <!--BOF Related Options-->
			  <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block" <?php echo 'master-option-value="' . $option['master_option_value'] . '"'; ?>><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
			  <!--EOF Related Options-->
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <!--BOF Related Options-->
				<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" <?php echo 'master-option-value="' . $option['master_option_value'] . '"'; ?> />
				<!--EOF Related Options-->
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <!--BOF Related Options-->
				<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" <?php echo 'master-option-value="' . $option['master_option_value'] . '"'; ?> class="form-control" />
				<!--EOF Related Options-->
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <!--BOF Related Options-->
			<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> option" <?php echo 'master-option="' . $option['master_option'] . '"' . ' option="' . $option['option_id'] . '"'; ?> id="option-<?php echo $option['product_option_id']; ?>" >
						<!--EOF Related Options-->
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <!--BOF Related Options-->
				<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" <?php echo 'master-option-value="' . $option['master_option_value'] . '"'; ?> class="form-control" />
				<!--EOF Related Options-->
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>

                              <?php } ?>
<!-- BOF Related Options -->
			</div>
			<!-- EOF Related Options -->
                              <?php } ?>
                          <?php } ?>
                              <!-- end options -->

                    <?php if ($discounts) { ?>
                          <hr>
                           <div class="discounts">
                            <h4><i class="fa fa-plus"></i> Desconto por Quantidade</h4>
                             <ul>
                              <?php foreach ($discounts as $discount) { ?>
                              <li>• <?php echo $discount['quantity']; ?><?php echo $text_discount; ?><b><?php echo $discount['price']; ?></b>/un</li>
                              <?php } ?>
                            </ul>
                          </div>
                     <?php } ?>

                  </div><!-- boxProductPriceRight --> 
                <?php } ?>

                </div><!-- boxProductPrice -->

                      <?php if ($recurrings) { ?>
                      <hr>
                      <h3><?php echo $text_payment_recurring ?></h3>
                      <div class="form-group required">
                        <select name="recurring_id" class="form-control">
                          <option value=""><?php echo $text_select; ?></option>
                          <?php foreach ($recurrings as $recurring) { ?>
                          <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                          <?php } ?>
                        </select>
                        <div class="help-block" id="recurring-description"></div>
                      </div>
                      <?php } ?>

                      <div class="clearfix"></div>
                        <?php if(!$quantity == '0'){ ?>
                      <div class="row">
                        <div class="col-md-6">
                              <div class="itemDesc clearfix">
                                <div class="cont" style="width:100%;">
                                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                    <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-cart-product"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></button>
                                </div>
                              </div><!-- itemDesc -->                          
                        </div><!-- col-md-6 -->
                        <div class="col-md-6">
                          <a href="<?php echo $goCart; ?>" class="btn btn-default btn-product"><i class="fa fa-shopping-bag"></i> Ir para o Carrinho</a>
                        </div><!-- col-md-6 -->
                     </div><!-- row -->
                      <?php }  ?>



<?php if($q > 0) { ?>
<?php if(isset($sem_opcao)) { ?> 
<br>
<?php }} ?>

 <?php if(((isset($sem_opcao)) or ($q <= 0)) and (isset($conf341->c5)) and ($conf341->c5 == 1) and (isset($conf341->c1)) and (!empty($conf341->c1)) and (isset($conf341->c2)) and (!empty($conf341->c2))) { ?>
<style>
.c21 {
font-size: 26px;
color: #FE8600;
margin-bottom: 6px;
}
</style>
<div id='conf341_aviso'></div>
<div id='conf341_div' class="well well-xs">
<form id="conf341_form" action="<?php echo $conf341_link; ?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_produto" value="<?php echo $product_id; ?>"/>
<input type="hidden" name="imagem" value="<?php echo $thumb; ?>"/>
<input type="hidden" name="product" value="<?php echo $heading_title; ?>"/>

<div class='c21'><?php echo $conf341->c1; ?></div>
<?php if((isset($conf341->c4)) and (!empty($conf341->c4))){ echo $conf341->c4; } ?>
  <div class="form-group">
<?php 
if(isset($options) and !empty($options)) { ?>
    <label for="email"><b>Escolha uma Opção</b></label>

<?php $c1 = 0; foreach ($options as $option) { ?>
  <?php if ($option['type'] == 'radio') { ?>
                    <div id="input-option<?php echo $option['product_option_id']; ?>">
                    <b><?php echo $option['name']; ?>:</b><br />
                    <?php foreach ($option['product_option_value'] as $option_value) { ?>

                    <?php if($option_value['code_quantidade'] <= 0) { ?>
                    <input class='code_caixinha' type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['name'].': '.$option_value['name'].'@'.$option_value['product_option_value_id']; ?>" id="codeoption-value-<?php echo $option_value['product_option_value_id']; ?>" <?php echo 'master-option-value="' . $option_value['master_option_value'] . '"' . ' option-value="' . $option_value['option_value_id'] . '"'; ?> />
                    <label class='caxinha' for="codeoption-value-<?php echo $option_value['product_option_value_id']; ?>">
                        <?php echo $option_value['name']; ?>
                    </label>
<?php } ?>
                    <?php } ?>
                    </div>
                    <?php } } } ?>
    <label for="email"><b>Informe seu E-mail</b></label>
    <input type="email" class="form-control" name="email">
  </div>
  <button id='conf341_button' type="submit" class="btn btn-purple"  data-loading-text="Enviando..."><?php echo $conf341->c2; ?></button>
</form>
</div>
<br>

<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.validate.min.js"></script>

<script type="text/javascript">
$( document ).ready(function() {
          $("#conf341_form").validate({
     rules: {
                email: {
                    required:true,
                     email: true,
                }
            },
    messages:{
      email:{
        required: "Digite o seu e-mail para contato",
        email: "Digite um e-mail válido",
      }
    },
      submitHandler: function (form) { 
        var b =  $("#conf341_button");
        b.button('loading');
         var dados = $('#conf341_form').serialize();
    $.ajax({
        type: "POST",
        url: $('#conf341_form').attr("action"),
        data: dados,
        success: function (data) {
            $("#conf341_div").hide("slow");
            $('#conf341_aviso').show('slow').html("<div class='alert alert-success'>"+data+"</div>");
            b.remove();
      },
      error: function(e){
                $('#conf341_aviso').show('slow').html("<div class='alert alert-error'>Houve um Problema para enviar o Avise-me.</div>");
                 b.button('reset');
            }

      });
return false;
           }
    });

    });
</script>
<?php } ?>

                      <ul class="ulWishCompare">
                        <li><a onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i> <?php echo $text_whistlist; ?></a></li>
                        <li><a onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i> <?php echo $button_compare; ?></a></li>
                      </ul>
                      <hr>   
                      <?php if ($minimum > 1) { ?>
                      <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                      <?php } ?>
                      <div id="limite"></div>

    <?php if ($review_status) { ?>
    <div class="rating">
     <?php if ($rating) { ?>
     <div class="star-rating star-<?php echo $rating;?>"></div>
     <?php } else { ?>
     <div class="star-rating star-0"></div>
     <?php } ?>  
     <span class=""><a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); $('body, html').animate({ scrollTop: 1000 }, 'slow'); return false;"><?php echo $reviews; ?></a> |
      <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); $('body, html').animate({ scrollTop: 1000 }, 'slow'); return false;" class="rate-this"><?php echo $text_write; ?></a></span>
    </div><!-- rating -->
    <?php } ?>

    <?php if ($tags) { ?> 
    <!-- tags -->  
    <hr>      
      <div class="productTags"><i class="fa fa-tags"></i>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </div><!-- productTags -->
    <!-- tags -->
    <?php } ?>

  </div><!-- col-md-4 -->

</div><!-- row -->


<?php if ($products) { ?>
<hr>            
<!-- produto relacionados -->
<section class="products">
  <header class="head familyOs">
    <h2><b><?php echo $text_related; ?></b></h2>
  </header>
  <?php $i = 0; ?>
  <ul class="products item-back-display style3 col-grid-4 space-30 clearfix">
    <?php foreach ($products as $product) { ?>    
    <li class="product text-center">
      <div class="productImages">
        <div class="image-default"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
      </div>
      <div class="productInfo">
        <h3><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>

        <?php if ($product['price']) { ?>
        <p><small><b>A partir de:</b></small></p>
        <ul class="pricestyle">
          <?php if (!$product['special']) { ?>
          <li><span class="priceReal"><?php echo $product['price']; ?></span></li>
          <?php } else { ?>
          <li>De: <strike><?php echo $product['price']; ?></strike></li>
          <li>Por: <span class="priceReal"><?php echo $product['special']; ?></span></li>
          <?php } ?> 
        </ul>
        <?php } ?>
        
        <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:80%; margin:.5em auto;' />";}?>
        <?php if (!$product['quantity'] <= '0') { ?>
        <div class="btn-group btn-group-justified" role="group">

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-purple" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></button>
          </div>

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-default" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_details; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';"><i class="fa fa-shopping-cart"></i> <?php echo $button_details; ?></button>
          </div>

        </div>
        <?php }else{ ?>

        <span class="alert alert-danger alert-nostock"><?php echo $text_instock; ?></span>

        <div class="button-group">
          <button type="submit" class="btn btn-purple" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_visit; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';" style="width:100%; margin:0 10%;"><i class="fa fa-shopping-cart"></i> <?php echo $button_visit; ?></button>
        </div>

        <?php } ?>

      </div>
    </li>
    <?php $i++; ?>
    <?php } ?>
  </ul>
</div>
</section>
<!-- produto relacionados -->
<?php } ?>


  <div class="row">
    <div class="col-xs-12  col-md-12">
      <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <?php  if($description == "<p><br></p>") {}else{ ?>
          <?php if ($description) {?><li role="presentation" class="hide active"><a href="#tabDescriptions" role="tab" data-toggle="tab"><i class="fa fa-book"></i> <?php echo $tab_description; ?></a></li><?php } ?>
          <?php } ?>
          <?php if ($review_status) { ?><li role="presentation"><a href="#tab-review" role="tab" data-toggle="tab"><i class="fa fa-comments"></i> <?php echo $tab_review; ?></a></li><?php } ?>
          <?php if ($attribute_groups) { ?> <li role="presentation"><a href="#tab3" role="tab" data-toggle="tab"><i class="fa fa-bars"></i> <?php echo $tab_attribute; ?></a></li> <?php } ?>
          <?php if(!$quantity == '0'){ ?><li role="presentation" class="hide"><a href="#tab4" role="tab" data-toggle="tab"><i class="fa fa-barcode"></i> Simulador de Parcelas</a></li><?php } ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">



          <?php if ($review_status) { ?>
          <div class="tab-pane" id="tab-review">
            <form class="form-horizontal">
              <div id="review"></div>
              <h2><?php echo $text_write; ?></h2>
              <?php if ($review_guest) { ?>
              <div class="form-group required">
                <div class="col-sm-12">
                  <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                  <input type="text" name="name" value="" id="input-name" class="form-control" />
                </div>
              </div>
              <div class="form-group required">
                <div class="col-sm-12">
                  <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                  <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                  <div class="help-block"><?php echo $text_note; ?></div>
                </div>
              </div>
              <div class="form-group required">
                <div class="col-sm-12">
                  <label class="control-label"><?php echo $entry_rating; ?></label>
                  &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                  <input type="radio" name="rating" value="1" />
                  &nbsp;
                  <input type="radio" name="rating" value="2" />
                  &nbsp;
                  <input type="radio" name="rating" value="3" />
                  &nbsp;
                  <input type="radio" name="rating" value="4" />
                  &nbsp;
                  <input type="radio" name="rating" value="5" />
                  &nbsp;<?php echo $entry_good; ?></div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-captcha"><?php echo $entry_captcha; ?></label>
                    <input type="text" name="captcha" value="" id="input-captcha" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12"> <img src="index.php?route=tool/captcha" alt="" id="captcha" /> </div>
                </div>
                <div class="buttons">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
            <?php } ?>

            <?php if ($attribute_groups) { ?>
            <div role="tabpanel" class="tab-pane" id="tab3">
              <table class="table table-bordered table-striped table-hover">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <th colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></th>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div><!-- tab-pane -->
            <?php } ?> 

<?php if(!$quantity == '0'){ ?>
<div role="tabpanel" class="tab-pane" id="tab4">
 <!-- simular pagamentos -->
 <div class="clearfix"></div>
 <div class="simuParcelar">
  <h4><i class="fa fa-barcode"></i> Simulador de Parcelas</h4>
  <div class="panel-group" id="accordion">
    <!-- Parcelamento Deposito -->
    <?php if (!$special) { ?>
    <?php $parcelamento_desconto = $price; ?>
    <?php } else { ?>
    <?php $parcelamento_desconto = $special; ?>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title" style="cursor:default;">
          <img src="catalog/view/images/exatos/deposito.png" alt="Depósito" />
          <span class="price-old-live"><?php echo $parcelamento_desconto; ?></span>
        </h4>
      </div><!-- panel-heading -->
    </div><!-- panel -->
    <!-- Parcelamento Deposito -->

    <?php if (!$special) { ?>
    <?php $preco_sem_sinal = str_replace(',','.',str_replace('.','', str_replace("R$","",$price))); ?>
    <?php } else { ?>
    <?php $preco_sem_sinal = str_replace(',','.',str_replace('.','', str_replace("R$","",$special))); ?>
    <?php } ?>
    <!-- Parcelamento PagSeguro -->
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#pagseguro">
          <img src="catalog/view/images/exatos/pagseguro.png" alt="PagSeguro" />
          <span><small>* clique e veja as parcelas</small></span>
        </h4>
      </div><!-- panel-heading -->
      <div id="pagseguro" class="panel-collapse collapse">
        <div class="panel-body">

          <table class="table table-hover table-striped table-bordered table-parcelamento">
            <thead>                      
              <tr><th>Parcelas</th><th>Valor</th></tr></thead>
              <tbody>
                <?php              
                $valor = $preco_sem_sinal;
                $max = '5,00';
                $var = parcelamento_pagseguro($valor,$max);                      
                ?>
              </tbody>
              <tfoot><tr><th colspan="2" align="left"><small>* Parcela mínima: R$ 5,00</small></th><tr></tfoot>
            </table>

          </div><!-- panel-body -->
        </div><!--pagseguro -->
      </div><!-- panel -->
      <!-- Parcelamento PagSeguro -->

    </div> <!-- panel accordion -->
    </div><!-- simuParcelar -->
  </div><!-- tab-pane -->
    <!-- end simular pagamentos -->
    <?php }//end if quantity == 0 ?>




          </div><!-- tab-content -->
        </div> <!-- Role Tabs -->
      </div><!-- col-md-7 -->
    </div><!-- row -->
  </div><!-- #product -->
</div><!-- row -->

</div><!-- basicDetails -->

</section>
<?php echo $content_bottom; ?></div>
<?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
  $.ajax({
    url: 'index.php?route=product/product/getRecurringDescription',
    type: 'post',
    data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
    dataType: 'json',
    beforeSend: function() {
      $('#recurring-description').html('');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();
      if (json['success']) {
        $('#recurring-description').html(json['success']);
      }
    }
  });
});

//--></script> 

<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
  $.ajax({
    url: 'index.php?route=checkout/cart/add',
    type: 'post',
    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-cart').button('loading');
    },
    complete: function() {
      $('#button-cart').button('reset');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();
      $('.form-group').removeClass('has-error');

      if (json['error']) {
        if (json['error']['option']) {
          for (i in json['error']['option']) {
            var element = $('#input-option' + i.replace('_', '-'));

            if (element.parent().hasClass('input-group')) {
              element.parent().after('<div class="alert alert-danger"><i class="fa fa-remove"></i> ' + json['error']['option'][i] + '</div>');
            } else {
              element.after('<div class="alert alert-danger"><i class="fa fa-remove"></i> ' + json['error']['option'][i] + '</div>');
            }
          }
        }
        if (json['error']['recurring']) {
          $('select[name=\'recurring_id\']').after('<div class="alert alert-danger"><i class="fa fa-remove"></i> ' + json['error']['recurring'] + '</div>');
        }
      // Highlight any found errors
      $('.alert alert-danger').parent().addClass('has-error');
    }
    if (json['success']) {
      $('.breadcrumb').after('<div class="container"><div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
      $('#cart-total').html(json['total']);
      $('html, body').animate({ scrollTop: 0 }, 'slow');
      $('#cart > div').load('index.php?route=common/cart/info div div');
    }
  }
});
});
//--></script> 

<script type="text/javascript"><!--
<?php if(!$quantity == '0'){ ?>
$('.date').datetimepicker({
  pickTime: false
});
$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});
$('.time').datetimepicker({
  pickDate: false
});
<?php } ?>
$('button[id^=\'button-upload\']').on('click', function() {
  var node = this;
  $('#form-upload').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-upload input[name=\'file\']').trigger('click');
  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);
      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);
            $(node).parent().find('input').attr('value', json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script> 
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();
  $('#review').fadeOut('slow');
  $('#review').load(this.href);
  $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
$('#button-review').on('click', function() {
  $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
    type: 'post',
    dataType: 'json',
    data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
    beforeSend: function() {
      $('#button-review').button('loading');
    },
    complete: function() {
      $('#button-review').button('reset');
      $('#captcha').attr('src', 'index.php?route=tool/captcha#'+new Date().getTime());
      $('input[name=\'captcha\']').val('');
    },
    success: function(json) {
      $('.alert-success, .alert-danger').remove();

      if (json['error']) {
        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }
      if (json['success']) {
        $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
        $('input[name=\'name\']').val('');
        $('textarea[name=\'text\']').val('');
        $('input[name=\'rating\']:checked').prop('checked', false);
        $('input[name=\'captcha\']').val('');
      }
    }
  });
});
<?php if(!$quantity == '0'){ ?>
$(document).ready(function() {
  $('.thumbnails').magnificPopup({
    type:'image',
    delegate: 'a',
    gallery: {
      enabled:true
    }
  });
});
<?php } ?>
//--></script> 

            <script type="text/javascript" src="index.php?route=product/live_options/js&product_id=<?php echo $product_id; ?>"></script>
            
<!--BOF Related Options-->
			<script type="text/javascript"><!--
				$(document).ready(function(){
					$('.option[master-option!=0]').hide();

					// SELECT OPTION
					$('.option select').bind('change', function(){
						var option_value = $(this).children('option:selected').attr('option-value');
						var $related_options = $('.option[master-option=' + $(this).parent('.option').attr('option') + ']');
						if ($(this).val() != '') {
							updateOptionList($related_options, option_value);
						} else {
							$related_options.slideUp();
						}
						clearOption($related_options);
					});

					// RADIO OPTION
					$('.option input[type=radio]').bind('change', function(){
						var option_value = $(this).attr('option-value');
						var $related_options = $('.option[master-option = ' + $(this).parents('.option').attr('option') + ']');
						if ($('input[name=\'' + $(this).attr('name') + '\']:checked').size() != '') {
							updateOptionList($related_options, option_value);
						} else {
							$related_options.slideUp();
						}
						clearOption($related_options);
					});

					// CHECKBOX OPTION
					$('.option input[type=checkbox]').bind('change', function(){
						var option_value = [];
						$('.option input[name=\'' + $(this).attr('name') + '\']:checked').each(function(){
							option_value.push($(this).attr('option-value'));
						});
						var $related_options = $('.option[master-option = ' + $(this).parents('.option').attr('option') + ']');
						if ($('input[name=\'' + $(this).attr('name') + '\']:checked').size() != '') {
							updateOptionList($related_options, option_value);
						} else {
							$related_options.slideUp();
						}
						clearOption($related_options);
					});

					// TEXT INPUT AND TEXTAREA OPTION
					$('.option input[type=text], .option textarea').bind('change', function(){
						var $related_options = $('.option[master-option = ' + $(this).parent('.option').attr('option') + ']');
						if ($(this).val() != '') {
							$related_options.slideDown();
						} else {
							$related_options.slideUp();
						}
						clearOption($related_options);
					});
				});
				function updateOptionList($related_options, option_value) {
					// select options
					$related_options.find('option[master-option-value!=0]').each(function(){
						if ($(this).parent('span').size() == 0) {
							$(this).wrap("<span>");
						}
					});
					$related_options.find('input[master-option-value!=0]').hide();
					$related_options.find('input[master-option-value!=0]').parent('label').parent('div').hide();
					$related_options.find('input[master-option-value!=0]').next('label').next('br').hide();
					$related_options.find('textarea[master-option-value!=0]').hide();
					// image options
					$related_options.find('input[master-option-value!=0]').each(function(){
						if ($(this).closest('table').hasClass('option-image')) {
							$(this).closest('tr').hide();
						}
					});


					if (typeof(option_value) == "string") {
						option_value = [option_value];
					}
					for (var i in option_value) {
						$related_options.find('option[master-option-value=' + option_value[i] + '], option[value=\'\']').each(function(){
							if ($(this).parent('span').size() != 0) {
								$(this).unwrap();
							}
						});
						$related_options.find('input[master-option-value=' + option_value[i] + ']').show();
						$related_options.find('input[master-option-value=' + option_value[i] + ']').parent('label').parent('div').show();
						$related_options.find('input[master-option-value=' + option_value[i] + ']').next('label').next('br').show();
						$related_options.find('textarea[master-option-value=' + option_value[i] + ']').show();
						// image options
						$related_options.find('input[master-option-value=' + option_value[i] + ']').each(function(){
							if ($(this).closest('table').hasClass('option-image')) {
								$(this).closest('tr').show();
							}
						});
					}

					$related_options.each(function(){
						var visible_options = 0;
						for (var i in option_value) {
							visible_options += $(this).find('[master-option-value=' + option_value[i] + ']').size()*1;
							visible_options += $(this).find('[master-option-value=0]').size()*1;
						}
						if ($(this).find('option, input, textarea').size() != 0 && visible_options == 0) {
							$(this).slideUp();
						} else {
							$(this).slideDown();
						}
					});
				}
				function clearOption($option_container) {
					$option_container.find('select').val('');
					$option_container.find('input[type=radio], input[type=checkbox]').removeAttr('checked');
					//$option_container.find('input[type=text], textarea').attr('value', '');
                    recalcPrice();
				}
			//--></script>
			<?php
				global $config;
				$related_options = $config->get('related_options');
				if (isset($related_options['price_adjustment_on']) && $related_options['price_adjustment_on']) {
			?>
			<script type="text/javascript"><!--
				// changing price
				var num = <?php echo $related_options['price_residue'] ?>;
				var special = <?php echo (!$special) ? 'false' : 'true'; ?>;
				var $price = $('.product-info .price:first');

				var regex_price = /[0-9]+[\.]{0,1}[0-9]*/;
				var regex_add_price = /([\+\-])[^0-9]*([0-9]+[\.]{0,1}[0-9]*)/;
				var regex_replace_price = /([0-9]+[\.]{0,1}[0-9]*)/;
				var initial_price = regex_price.exec('<?php echo (!$special) ? str_replace($decimal_point, ".", str_replace($thousand_point, "", $price)) : str_replace($decimal_point, ".", str_replace($thousand_point, "", $special)); ?>');

                function formatMoney(n, c, d, t) {
                    var c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                    j = (j = i.length) > 3 ? j % 3 : 0;
                   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
                };

				function recalcPrice() {
					total_price = initial_price[0]*1;
					$('.options input[type=radio]:checked, .options input[type=checkbox]:checked').each(function(){
						additional_price = regex_add_price.exec($('label[for=' + $(this).attr('id') + '] span').text().replace('<?php echo $decimal_point; ?>', '.').replace(new RegExp('<?php echo $thousand_point; ?>', 'g'), ''));
						if (additional_price != null && additional_price != '' && additional_price != undefined) {
							if (additional_price[1] == '+') {
								total_price = total_price*1 + additional_price[2]*1;
							} else if (additional_price[1] == '-') {
								total_price = total_price*1 - additional_price[2]*1;
							}
						}
					});
					$('.options select>option:selected').each(function(){
						additional_price = regex_add_price.exec($(this).text().replace('<?php echo $decimal_point; ?>', '.').replace(new RegExp('<?php echo $thousand_point; ?>', 'g'), ''));
						if (additional_price != null && additional_price != '' && additional_price != undefined) {
							if (additional_price[1] == '+') {
								total_price = total_price*1 + additional_price[2]*1;
							} else if (additional_price[1] == '-') {
								total_price = total_price*1 - additional_price[2]*1;
							}
						}
					});
					if (special) {
						$price.find('.price-old').remove();
					}

					total_price = formatMoney(total_price, num, '<?php echo $decimal_point; ?>', '<?php echo $thousand_point; ?>');
					//total_price = total_price.toFixed(num);

					$price.html($price.html().replace('<?php echo $decimal_point; ?>', '.').replace(new RegExp('<?php echo $thousand_point; ?>', 'g'), '').replace(regex_replace_price, total_price));
				}
			//--></script>
			<?php } else { ?>
			<script type="text/javascript"><!--
				function recalcPrice() {
					return true;
				}
			//--></script>
			<?php } ?>
			<!--EOF Related Options-->
<?php echo $footer; ?>