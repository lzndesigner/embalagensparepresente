<!DOCTYPE html>

<!--[if IE]><![endif]-->

<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->

<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->

<!--[if (gt IE 9)|!(IE)]><!-->

<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<!--<![endif]-->

<head>

  <meta charset="UTF-8" />

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php echo $title; ?></title>

  <base href="<?php echo $base; ?>" />
  <meta name="robots" content="follow" />
  <meta name="googlebot" content="index, follow, all" />
  <meta name="language" content="pt-br" />
  <meta name="revisit-after" content="3 days">
  <meta name="rating" content="general" />
  <meta property="og:locale" content="pt_BR"/>
  <meta property="og:type" content="website"/>
  <?php if($fbMetas){ ?>
    <?php foreach ($fbMetas as $fbMeta) { ?>
      <meta property="og:image:url" content="<?php echo $base; ?>image/<?php echo $fbMeta['content']; ?>" />
      <meta property="og:image:type" content="image/jpeg" />
    <?php } ?>
  <?php }else{ ?>
    <meta property="og:image:url" content="<?php echo $base; ?>facebook.jpg" />
    <meta property="og:image:type" content="image/jpeg" />
  <?php } ?>

  <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <meta property="og:description" content="<?php echo $description; ?>"/>
  <?php } ?>

  <?php if ($keywords) { ?>

    <meta name="keywords" content= "<?php echo $keywords; ?>" />

  <?php } ?>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <?php if ($icon) { ?>

    <link href="<?php echo $base; ?>image/catalog/favicon.ico" rel="icon" />

  <?php } ?>

  <?php foreach ($links as $link) { ?>

    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />

  <?php } ?>



  <!-- Requerido Template Novo --> 

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/custom-icons.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/odometer-theme-default.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/slick.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/prettyPhoto.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/animate.css">
  <link rel="stylesheet" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/main.css?21">

  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

  <!--- jQuery -->
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery-1.11.3.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/packery.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/masonry.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/isotope.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.stellar.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/slick.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.inview.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/odometer.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/tweetie.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.timeago.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.knob.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/css3-animate-it.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/imagesloaded.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.prettyPhoto.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/main.js"></script>
  <!-- Requerido Template Novo -->
  <script src='catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.elevatezoom.js'></script>



  <link href="catalog/view/theme/<?php echo $usedTpl; ?>/stylesheet/stylesheet.css?1" rel="stylesheet" type="text/css" />

  <?php foreach ($styles as $style) { ?>

    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />

  <?php } ?>

  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/common.js" type="text/javascript"></script>
  <?php foreach ($scripts as $script) { ?>

    <script src="<?php echo $script; ?>" type="text/javascript"></script>

  <?php } ?>

  <?php echo $google_analytics; ?>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-101895634-2', 'auto');
    ga('send', 'pageview');
  </script>

  <!-- Global site tag (gtag.js) - Google Ads: 797622369 -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-797622369"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-797622369');
  </script>
  <?php if($class == "checkout-success"){ ?>
    <!-- Event snippet for Conversão conversion page -->
    <script>
      gtag('event', 'conversion', {
        'send_to': 'AW-797622369/maYsCIbRtogBEOGAq_wC',
        'transaction_id': ''
      });
    </script>
  <?php } ?>


  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1525647790825093'); 
      fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1" 
     src="https://www.facebook.com/tr?id=1525647790825093&ev=PageView
     &noscript=1"/>
   </noscript>
   <!-- End Facebook Pixel Code -->


   <script src="catalog/view/javascript/jquery.maskedinput.js"></script>
   <script>
    $(document).ready(function(){
      $('#input-telephone').mask('(00) 0000-00009');
      $('#input-telephone').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-telephone').mask('(00) 0000-0000');
        } else {
          $('#input-telephone').mask('(00) 90000-0000');
        }
      });
      $('#input-fax').mask('(00) 0000-00009');
      $('#input-fax').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-fax').mask('(00) 0000-0000');
        } else {
          $('#input-fax').mask('(00) 90000-0000');
        }
      });
      $('#input-payment-telephone').mask('(00) 0000-00009');
      $('#input-payment-telephone').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-payment-telephone').mask('(00) 0000-0000');
        } else {
          $('#input-payment-telephone').mask('(00) 90000-0000');
        }
      });
      $('#input-payment-fax').mask('(00) 0000-00009');
      $('#input-payment-fax').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-payment-fax').mask('(00) 0000-0000');
        } else {
          $('#input-payment-fax').mask('(00) 90000-0000');
        }
      });
      $('#telefone').mask('(00) 0000-00009');
      $('#telefone').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#telefone').mask('(00) 0000-0000');
        } else {
          $('#telefone').mask('(00) 90000-0000');
        }
      });

      $("#input-tax").mask("999.999.999-99");

      $("#input-custom-field1").mask("00.000.000/0000-00");
      $('#input-custom-field1').blur(function(event) {           
        $('#input-custom-field1').mask('00.000.000/0000-00');          
      });

      $("#input-payment-custom-field1").mask("00.000.000/0000-00");
      $('#input-payment-custom-field1').blur(function(event) {         
        $('#input-payment-custom-field1').mask('00.000.000/0000-00');        
      });
    });
  </script>

<?php 
if($class != "checkout-checkout"){ ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59a5bbf6e8b4ce6a1a6d0ec7/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<?php }?>

<script src="https://assets.pagar.me/checkout/checkout.js"></script>
</head>

<body class="<?php echo $class; ?>">

  <?php if($class == "checkout-success"){ ?>
    <!-- Google Code for Venda Conversion Page -->
    <script type="text/javascript">
      /* <![CDATA[ */
      var google_conversion_id = 847161452;
      var google_conversion_language = "en";
      var google_conversion_format = "3";
      var google_conversion_color = "ffffff";
      var google_conversion_label = "OWQbCPjS2HIQ7ND6kwM";
      var google_remarketing_only = false;
      /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
      <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/847161452/?label=OWQbCPjS2HIQ7ND6kwM&amp;guid=ON&amp;script=0"/>
      </div>
    </noscript>
  <?php } ?>


  <div class="bodyWrap">
    <header class="doc-header style-pink bgPurple">
      <div class="topHeader">
        <div class="container hidden-xs">
          <div class="row">
            <div class="col-md-12">
              <div class="infoAccounts pull-right">
                <ul>
                  <li><i class="fa fa-phone"></i> Atendimento: <b><?php echo $telephone; ?></b> <?php if($celular){ echo "|"; } ?> <i class="fa fa-whatsapp"></i> WhatsApp: <b><?php echo $celular; ?></b></li>
                  <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <?php echo $text_wishlist; ?></a></li>                
                  <?php if ($logged) { ?>
                    <li><i class="fa fa-user"></i> Minha Conta
                    <ul>
                     <li><a href="<?php echo $account; ?>"><i class="fa fa-user"></i> <?php echo $text_my_account; ?></a></li>
                     <li><a href="<?php echo $order; ?>"><i class="fa fa-book"></i> <?php echo $text_order; ?></a></li>
                     <li><a href="<?php echo $transaction; ?>"><i class="fa fa-credit-card"></i> <?php echo $text_transaction; ?></a></li>
                     <li><a href="<?php echo $logout; ?>"><i class="fa fa-sign-out"></i> <?php echo $text_logout; ?></a></li>
                   </ul>
                 </li>
               <?php } else { ?>
                 <li><a href="<?php echo $login; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $text_my_account; ?>"><i class="fa fa-user"></i> <?php echo $text_my_account; ?></a></li>
                 <li><a href="<?php echo $register; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $text_register; ?>"><i class="fa fa-address-card-o"></i> <?php echo $text_register; ?></a></li>
               <?php } ?>                    
             </ul>
           </div><!-- infoTop -->
         </div>
       </div>
     </div>



     <div class="container">  
      <div class="row">

        <div class="col-xs-12 col-md-4">
          <ul class="logo">
            <li>
              <a href="<?php echo $home; ?>">
               <?php if ($logo) { ?>
                 <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="logo-dark" />
               <?php } else { ?>
                 <h1><?php echo $name; ?></h1>
               <?php } ?>
             </a>                
           </li>
         </ul>
       </div><!-- col-md-4 -->

       <div class="col-xs-12 col-md-8 secondMenuTop ">
        <div class="row">
          <div class="col-xs-12 col-md-8">
            <div id="quick-access">
              <?php echo $search; ?>
            </div><!-- End #quick-access --> 
          </div><!-- col-md- 8-->
          <div class="col-xs-12 col-md-4">
            <div class="topLinks pull-right">
              <?php echo $cart; ?>
            </div>
            <div class="clearfix"></div>
            <ul class="lngcur">
              <?php echo $currency; ?>
              <?php echo $language; ?>
            </ul>                
          </div><!-- col-md-4 -->
        </div><!-- row -->

      </div><!-- col-md-8 -->
    </div> <!-- row -->     

    <div class="row hidden-xs">
      <div class="col-md-12">
        <div class="banners-selos clearfix">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="icon-selos"><i class="fa fa-money"></i></div>
            <div>
              <h2>5% de Desconto</h2>
              <p>Pagamento no Depósito</p>
            </div><!-- -->
          </div>                
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="icon-selos"><i class="fa fa-credit-card"></i></div>
            <div>
              <h2>Pagamento Fácil</h2>
              <p>Suas Compras em Até <?php echo $config_limit_parcela; ?>X.</p>
            </div><!-- -->
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="icon-selos"><i class="fa fa-truck"></i></div>
            <div>
              <h2>Frete grátis SUL & SUDESTE</h2>
              <p>Nas compras acima de R$ 300,00 via PAC</p>
            </div><!-- -->
          </div><!-- col-md-3 -->
          <!-- col-md-3 -->
        </div><!-- banners-selos -->
      </div><!-- col-md-12 -->
    </div><!-- row -->

    <div class="row">
      <div class="col-xs-12 col-md-12">
        <ul class="topLeftLinks">
          <li><a class="navTriger" href="#" title="Abrir Menu de Departamentos" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-bars"></i> Menu</a></li>
          <li><a class="cartBtn" href="index.php?route=checkout/cart" title="<?php echo $text_shopping_cart; ?>" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-shopping-cart"></i></a></li>
          <li><a class="navTriger" href="<?php echo $login; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $text_my_account; ?>"><i class="fa fa-user"></i> </a></li>
        </ul> 
        <nav class="navMain">
          <div class="container">
            <ul class="mainNav">
              <?php if ($categories) { ?>

                <?php foreach ($categories as $category) { ?>

                  <?php if ($category['children']) { ?>

                    <li class="<?php if($category['column'] > 1){ echo "megaparent";}?>"><a href="<?php echo $category['href']; ?>"  title="<?php echo $category['name']; ?>" 

                      <?php if($category['name'] == "Laço Pronto para Presente"){ echo "class='menu_1'"; }
                      elseif($category['name'] == "Sacola Para Presente"){ echo "class='menu_2'";}
                      elseif($category['name'] == "Sacos"){ echo "class='menu_3'";}
                      elseif($category['name'] == "Caixa Para Presente"){ echo "class='menu_4'";}
                      elseif($category['name'] == "Organzas"){ echo "class='menu_5'";}
                      ?>><span class="hidden-lg hidden-sm hidden-md"><?php echo $category['name'] ; ?></span></a> 

                      <?php if($category['column'] > 1){ ?>
                        <div class="mega-xv">
                          <div class="container">
                            <div class="row">
                            <?php }else{ ?>
                              <ul>
                              <?php } ?>
                              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                <?php if($category['column'] > 1){ ?>
                                  <div class="col-xs-12 col-sm-6 col-md-3">
                                    <ul>
                                    <?php } ?>

                                    <?php foreach ($children as $child) { ?>
                                      <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?>  <?php if($child['children_lv3']){?><i class="fa fa-angle-right hidden-xs" style="float: right; margin: 2px;"></i><?php } ?></a></li>
                                    <?php } ?>
                                    <?php if($category['column'] > 1){ ?>
                                    </ul>
                                  </div>
                                <?php } ?>

                              <?php } ?>    
                              <?php if($category['column'] > 1){ ?>
                              </div>
                            </div>
                          </div>
                        <?php }else{ ?>
                        </ul>
                      <?php } ?>

                    <?php } else { ?>

                      <li><a href="<?php echo $category['href']; ?>"  title="<?php echo $category['name']; ?>" 

                        <?php if($category['name'] == "Organzas"){ echo "class='menu_5'";} ?>><span class="hidden-lg hidden-sm hidden-md"><?php echo $category['name'] ; ?></span></a> </li>

                      <?php } ?>

                    <?php } ?>
                  <?php } ?>

                  <!-- <li class="promotion"><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li> -->
                </ul>
              </div>
            </nav>  
          </div>
        </div> 
      </div><!-- container -->
    </div><!-- top header -->
  </header>
  <div class="clearfix"></div>