<?php
// Heading
$_['heading_title'] = 'Veja Mais...';

// Text
$_['text_tax']      = 'Sem impostos:';
$_['text_instock']      = 'ESGOTADO';
$_['button_visit']      = 'Notifique-me';
$_['button_details']        = 'Detalhes';