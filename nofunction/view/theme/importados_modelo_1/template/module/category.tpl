 <aside>
  <div class="menuLateral">
    <h2 class="titleLateral"><?php echo $heading_title; ?></h2>
    <ul>     
      <?php foreach ($categories as $category) { ?> 
      
      <?php if ($category['children']) { ?>
      <li class="has-sub"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
        <ul >
          <?php foreach ($category['children'] as $child) { ?>            
          <li <?php if($child['children_lv3']){?>class="has-sub"<?php } ?>><a href="<?php echo $child['href']; ?>">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
                <?php if($child['children_lv3']){?>
                    <ul>                
                      <?php foreach ($child['children_lv3'] as $child_lv3) { ?>
                      <li><a href="<?php echo $child_lv3['href']; ?>"><?php echo $child_lv3['name']; ?></a></li>
                      <?php } ?>
                    </ul>
                <?php } ?>
          </li>            
          <?php } ?>
        </ul>        
      </li>
      <?php } else { ?>
      <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
      <?php }//end if ?>
      
      <?php }// end foreach ?>

    </ul>
  </div>

  <div class="menuLateral">
    <h2 class="titleLateral"><?php echo $heading_information; ?></h2>
    <ul>
      <li><a href="index.php?route=account/account"><?php echo $text_account; ?></a></li>
      <li><a href="index.php?route=information/contact"><?php echo $text_contact; ?></a></li>
      <li><a href="index.php?route=information/confirmarpagamento"><?php echo $text_confirm; ?></a></li>
      <li><a href="index.php?route=account/return/add"><?php echo $text_return; ?></a></li>     
      <li><a href="index.php?route=information/sitemap"><?php echo $text_map; ?></a></li>
    </ul>
  </div>
</aside>
