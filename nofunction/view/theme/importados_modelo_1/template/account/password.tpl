<?php echo $header; ?>



<div class="breadcrumb clearfix bgGrey">
  <div class="container">
    <h5><?php echo $heading_title; ?></h5>
    <ul class="">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div><!-- breadcrumb -->

<div class="container">


  <div class="row"><?php echo $column_left; ?>



    <?php if ($column_left && $column_right) { ?>



    <?php $class = 'col-sm-6'; ?>



    <?php } elseif ($column_left || $column_right) { ?>



    <?php $class = 'col-sm-9'; ?>



    <?php } else { ?>



    <?php $class = 'col-sm-12'; ?>



    <?php } ?>



    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>



            <h1 class="title"><?php echo $heading_title; ?></h1>     



            <p><?php echo $text_password; ?></p>     





      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">



        <fieldset>



       <ul style="padding:0 1.5em 1.5em; list-style:inside;">



        <?php if ($error_password) { ?>



          <li class="text-danger"><?php echo $error_password; ?></li>



        <?php } ?>



      </ul>



      <div class="itemDesc clearfix">
          <div class="name"><?php echo $entry_password; ?></label>
          </div>
          <div class="cont">
            <div class="custome-select grey clearfix">
              <input type="password" required name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
            </div>
          </div>
      </div><!-- itemDesc -->

      <div class="itemDesc clearfix">
          <div class="name"><?php echo $entry_confirm; ?></label>
          </div>
          <div class="cont">
            <div class="custome-select grey clearfix">
              <input type="password" required name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
            </div>
          </div>
      </div><!-- itemDesc -->



        </fieldset>



        <div class="buttons clearfix">



          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>



          <div class="pull-right">



            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-purple" />



          </div>



        </div>



      </form>




      <?php echo $content_bottom; ?></div>



    <?php echo $column_right; ?></div>



</div>



<?php echo $footer; ?> 