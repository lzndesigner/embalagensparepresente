<footer class="doc-footer">
    <div class="bgFooter bgWhite">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 boxLogoFooter">
                    <ul class="logo">
                        <li>
                            <a href="<?php echo $home; ?>">
                                <?php if ($logo) { ?>
                                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="logo-dark" />
                                <?php } else { ?>
                                <h1><?php echo $name; ?></h1>
                                <?php } ?>
                            </a>                
                        </li>
                        <li><p><?php echo $description; ?></p></li>
                    </ul> 



                </div>
                <div class="col-sm-3">
                    <h6><?php echo $text_account; ?></h6>
                    <ul class="links">
                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                        <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>

                    </ul>
                </div>
                <div class="col-sm-3">
                    <h6><?php echo $text_information; ?></h6>
                    <ul class="links">
                        <?php if ($informations) { ?>
                        <?php foreach ($informations as $information) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>
                        <?php } ?>            


                    </ul>
                </div>
                <div class="col-sm-3">
                    <h6><?php echo $title_helpdesk; ?></h6>
                    <ul class="links">
                        <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
                        <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                        <li><a href="index.php?route=information/confirmarpagamento"><?php echo $text_confirm_payment; ?></a></li>
                        <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                        <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <span class="copyrights">
                        <?php echo $enderecoLoja; ?>
                        <br/>
                        <?php echo $powered; ?><br/>
                    </span>
                    <hr>
                    <div class="col-sm-8">
                        <div class="boxPayments">
                            <ul>
                                <li><img src="catalog/view/images/cartoes/deposito.png" alt="Depósito Bancário"></li>
                                <li><img src="catalog/view/images/cartoes/mercadopago.png" alt="Mercado Pago"></li>
                                <li><img src="catalog/view/images/cartoes/pagamento_digital.png" alt="BCash Pagamento Digital"></li>
                                <li><img src="catalog/view/images/cartoes/pagseguro.png" alt="PagSeguro"></li>
                                <li><img src="catalog/view/images/cartoes/paypal.png" alt="PayPal"></li>
                            </ul>
                        </div>    
                    </div>
                    <div class="col-sm-4">
                        <b><?php echo $text_tecnologia; ?></b><br/>
                        <a href="http://www.innsystem.com.br" title="InnSystem - Inovação em Sistemas" target="_Blank">
                            <img src="http://www.innsystem.com.br/_galerias/logo.png" alt="InnSystem - Inovação em Sistemas" style="width:110px; margin:-18px 0 0 -2.8em;" />
                        </a>
                    </div>
                </div><!-- col-sm-12 -->        
            </div>
        </div>
    </div>

</footer>



<script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/bootstrap.min.js"></script>
<script type='text/javascript' src='catalog/view/javascript/jquery.cookie.js'></script>
<script>
$(function(){

    if($.cookie('box-pop-up') == null)
    {   
        $(document).keyup(function(e) {
if (e.keyCode == 27) buttonClose();  // esc
});

        $("#box-popup").click(function(e) { 
            e.stopPropagation();
        });

        $("#box-popup-ctn").click(function(e) { 
            buttonClose();
        });

        $.cookie('box-pop-up', 'box-pop', { expires: 2 });

        $('#box-popup-ctn').show();
    }


    if (top != window)
        $('#box-popup-ctn').hide();
});

function buttonClose(){
    $('#box-popup-ctn').fadeOut();
};
</script>
</div><!-- End .bodyWrap -->
</body></html>