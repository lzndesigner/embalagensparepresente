<div id="cart" class="dropdown-cart-menu-container pull-right">

  <button type="button" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-custom dropdown-toggle"><i class="fa fa-shopping-cart"></i> <span id="cart-total"><?php echo $text_items; ?></span></button>

  <ul class="dropdown-menu pull-right">

    <?php if ($products || $vouchers) { ?>



        <?php foreach ($products as $product) { ?>

            <li class="item clearfix">

                <button type="button" onclick="cart.remove('<?php echo $product['key']; ?>');" title="<?php echo $button_remove; ?>" class="delete-item"><i class="fa fa-times"></i></button>

            <?php if ($product['thumb']) { ?>

            <figure>

                    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>

                </figure>

            <?php } ?>

             <div class="dropdown-cart-details">

                    <p class="item-name">

                      <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>

                     <?php if ($product['option']) { ?>

                      <?php foreach ($product['option'] as $option) { ?>

                      <br />

                      - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>

                      <?php } ?>

                      <?php } ?>

                      <?php if ($product['recurring']) { ?>

                      <br />

                      - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>

                      <?php } ?>

                    </p>

                    <p>

                        <?php echo $product['quantity']; ?>x <?php echo $product['price']; ?><br/>

                        <span class="item-price"><?php echo $product['total']; ?></span>

                    </p>

                </div><!-- End .dropdown-cart-details -->



       

         </li>

        <?php } ?>

        <?php foreach ($vouchers as $voucher) { ?>

          <li class="item clearfix">

          <td class="text-center"></td>

          <td class="text-left"><?php echo $voucher['description']; ?></td>

          <td class="text-right">x&nbsp;1</td>

          <td class="text-right"><?php echo $voucher['amount']; ?></td>

          <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>

        

        </li>

        <?php } ?>

    <li>

       <ul class="dropdown-cart-total">

          <?php foreach ($totals as $total) { ?>

          <li><span class="dropdown-cart-total-title"><?php echo $total['title']; ?></span> <b><?php echo $total['text']; ?></b></li>

          <?php } ?>

        </ul>

  </li>
  <li>



            <div class="dropdown-cart-action">

                <a href="<?php echo $cart; ?>" class="btn btn-custom-2 btn-block"><?php echo $text_cart; ?></a>

                <a href="<?php echo $checkout; ?>" class="btn btn-custom btn-block" style="color:#FFF;"><?php echo $text_checkout; ?></a>

            </div><!-- End .dropdown-cart-action -->


    </li>

    <?php } else { ?>

    <li>

      <p class="text-center"><?php echo $text_empty; ?></p>

    </li>

    <?php } ?>

  </ul>

</div>

