
        <footer id="footer">

          <div id="inner-footer">

            

        <div class="container">

          <div class="row">

            <div class="col-md-3 col-sm-4 col-xs-12 widget">

               <h3><?php echo $text_account; ?></h3>

              <ul class="links">

                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>

                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>

                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>

                <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>

                <li><a href="index.php?route=information/confirmarpagamento">Confirmar Pagamento</a></li>

              </ul>              

            </div><!-- End .widget -->

            

            <div class="col-md-3 col-sm-4 col-xs-12 widget">

              <?php if ($informations) { ?>

              <h3><?php echo $text_information; ?></h3>

              <ul class="links">

                <?php foreach ($informations as $information) { ?>

                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>

                <?php } ?>

              </ul>

              <?php } ?>

              <ul class="links">

                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>

                <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>

                <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>

              </ul>


           




            </div><!-- End .widget -->



            <div class="col-md-3 col-sm-4 col-xs-12 widget">

              <h3>Entre em Contato</h3>

                <ul class="links">

            <li><a href="https://www.facebook.com/embalagensparapresente" title="Facebook embalagensparapresente" target="_blank" class="icon"><i class="fa fa-facebook"></i> <span class="">embalagensparapresente</span></a></li>

            <li><a title="Telefone (11) 3227-8304" class="icon"><i class="fa fa-phone"></i> <span class="">Telefone (11) 3227-5798</span></a></li>

            <li><a title="Whatsapp (11) 9-9732-6795" class="icon"><img src="catalog/view/theme/modelo_1/_galerias/whatsapp.png" width="15" alt=""> <span class="">Whatsapp (11) 9-9732-6795</span></a></li>

            <li><a title="Centro - São Paulo" class="icon"><i class="fa fa-home"></i> <span class="">Centro - São Paulo</span></a></li>

            </ul>

                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.814844992116!2d-46.63242538502261!3d-23.539160984693954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce58f85bc0c52d%3A0xbf431c82c323e437!2sCondom%C3%ADnio+Edif%C3%ADcio+Sylvia+-+R.+Miguel+Carlos%2C+41+-+Santa+Ifig%C3%AAnia%2C+S%C3%A3o+Paulo+-+SP%2C+01023-900!5e0!3m2!1spt-BR!2sbr!4v1501158819705" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div><!-- End .widget -->

            

            <div class="clearfix visible-sm"></div>

            

            <div class="col-md-3 col-sm-12 col-xs-12 widget">

              <h3>Facebook</h3>

              

              <div class="facebook-likebox">
  <div id="fb-root"></div>



      <script>(function(d, s, id) {



        var js, fjs = d.getElementsByTagName(s)[0];



        if (d.getElementById(id)) return;



        js = d.createElement(s); js.id = id;



        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";



        fjs.parentNode.insertBefore(js, fjs);



      }(document, 'script', 'facebook-jssdk'));</script>



      <div class="fb-like-box" data-href="https://www.facebook.com/embalagensparapresente" data-width="340" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>


              </div>

              

              

            </div><!-- End .widget -->

          </div><!-- End .row -->

        </div><!-- End .container -->

          

   </div><!-- End #inner-footer -->

          

          <div id="footer-bottom">

            <div class="container">

              <div class="row">

                

                <div class="col-md-12 col-sm-12 col-xs-12 footer-text-container">

                  <p><?php echo $powered; ?></p>

                </div><!-- End .col-md-5 -->

              </div><!-- End .row -->

        </div><!-- End .container -->

          </div><!-- End #footer-bottom -->

          

        </footer><!-- End #footer -->

    </div><!-- End #wrapper -->

        <a href="#" id="scroll-top" title="Ir para o Topo"><i class="fa fa-angle-up"></i></a><!-- End #scroll-top -->



    <script src="catalog/view/theme/modelo_1/_template/js/bootstrap.min.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/smoothscroll.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/retina-1.1.0.min.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/jquery.placeholder.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/jquery.hoverIntent.min.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/twitter/jquery.tweet.min.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/jquery.flexslider-min.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/owl.carousel.min.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/jflickrfeed.min.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/jquery.prettyPhoto.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/jquery.themepunch.plugins.min.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/jquery.themepunch.revolution.min.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/colpick.js"></script>

    <script src="catalog/view/theme/modelo_1/_template/js/main.js"></script>

  

    <script>

      $(function() {



        // Slider Revolution

            jQuery('#slider-rev').revolution({

                delay:5000,

                startwidth:1170,

                startheight:600,

                onHoverStop:"true",

                hideThumbs:250,

                navigationHAlign:"center",

                navigationVAlign:"bottom",

                navigationHOffset:0,

                navigationVOffset:12,

                soloArrowLeftHalign:"left",

                soloArrowLeftValign:"center",

                soloArrowLeftHOffset:0,

                soloArrowLeftVOffset:0,

                soloArrowRightHalign:"right",

                soloArrowRightValign:"center",

                soloArrowRightHOffset:0,

                soloArrowRightVOffset:0,

                touchenabled:"on",

                stopAtSlide:-1,

                stopAfterLoops:-1,

                dottedOverlay:"none",

                fullWidth:"on",

                shadow:0

         

              });

      

      });

    </script>
</body></html>