<?php if (count($languages) > 1) { ?>
<div class="btn-group dropdown-language">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
    <button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown">
        <?php foreach ($languages as $language) { ?>
        <?php if ($language['code'] == $code) { ?>
        <span class="flag-container"><img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>"></span>
        <span class="hide-for-xs"><?php echo $text_language; ?></span>
        <?php } ?>
        <?php } ?>
    </button>
    <ul class="dropdown-menu pull-right" role="menu">
      <?php foreach ($languages as $language) { ?>
      <li><a href="<?php echo $language['code']; ?>">
        <span class="flag-container"><img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /></span>
        <span class="hide-for-xs"><?php echo $language['name']; ?></span>
       </a></li>
      <?php } ?>
    </ul>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
</div><!-- End .btn-group -->
<?php } ?>
