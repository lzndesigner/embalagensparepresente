<?php if ($reviews) { ?>
<div class="comments">
  <header class="title-bg">
    <h3>Comentários de Clientes</h3>
  </header>
<ul class="comments-list">    

<?php foreach ($reviews as $review) { ?>

 <li>
  <div class="comment" style="padding-left:0;">
    <div class="comment-details">
      <div class="comment-meta-container">
        <b><?php echo $review['author']; ?></b>
        <span style="font-size:12px; padding-left:10px;"><?php echo $review['date_added']; ?></span>
      </div><!-- End .comment-meta-container -->
      <p><?php echo $review['text']; ?></p>
      <p style="font-size:8px;"><?php for ($i = 1; $i <= 5; $i++) { ?>

      <?php if ($review['rating'] < $i) { ?>

      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>

      <?php } else { ?>

      <span class="fa fa-stack" style="color:#000;"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>

      <?php } ?>

      <?php } ?></p>
    </div><!-- End .comment-details -->
  </div><!-- End .comment -->
</li>

<?php } ?>
</ul>
</div>
<div class="text-right"><?php echo $pagination; ?></div>

<?php } else { ?>

<p><?php echo $text_no_reviews; ?></p>

<?php } ?>

