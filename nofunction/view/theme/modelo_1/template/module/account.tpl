
      <aside class="col-md-12 col-sm-12 col-xs-12 sidebar">      



        <div class="panel-group custom-accordion sm-accordion" id="category-filter">


           <?php if (!$logged) { ?>


            <div class="panel">


                <div class="accordion-header">


                  <div class="accordion-title"><span>Minha Conta</span></div><!-- End .accordion-title -->


                    <a class="accordion-btn opened" data-toggle="collapse" data-target="#category-list-1"></a>


                  </div><!-- End .accordion-header -->


                


                  <div id="category-list-1" class="collapse in">


                      <div class="panel-body">


                      <ul class="category-filter-list jscrollpane">
                       

                        <li><a href="<?php echo $login; ?>"><i class="fa fa-user"></i> <?php echo $text_login; ?></a></li> 
                        <li><a href="<?php echo $register; ?>"><i class="fa fa-user-plus"></i> <?php echo $text_register; ?></a></li>
                        <li><a href="<?php echo $forgotten; ?>"><i class="fa fa-lock"></i> <?php echo $text_forgotten; ?></a></li>

                      </ul>


                  </div><!-- End .panel-body -->


                </div><!-- #collapse -->


              </div><!-- End .panel -->


                       

                        <?php } ?>



              <?php if ($logged) { ?>
              <div class="panel">


                <div class="accordion-header">


                  <div class="accordion-title"><span>Minha Conta</span></div><!-- End .accordion-title -->


                    <a class="accordion-btn opened" data-toggle="collapse" data-target="#category-list-2"></a>


                  </div><!-- End .accordion-header -->


                


                  <div id="category-list-2" class="collapse in">


                      <div class="panel-body">


                      <ul class="category-filter-list jscrollpane">
                       


                       <li><a href="<?php echo $edit; ?>"><i class="fa fa-user"></i> <?php echo $text_edit; ?></a></li>


                        <li><a href="<?php echo $password; ?>"><i class="fa fa-key"></i> <?php echo $text_password; ?></a></li>


                        <li><a href="<?php echo $address; ?>"><i class="fa fa-home"></i> <?php echo $text_address; ?></a></li>


                        <li><a href="<?php echo $wishlist; ?>"><i class="fa fa-heart"></i> <?php echo $text_wishlist; ?></a></li>

                        

                          <li><a href="<?php echo $logout; ?>"><i class="fa fa-sign-out"></i> <?php echo $text_logout; ?></a></li>

                          


                      </ul>


                  </div><!-- End .panel-body -->


                </div><!-- #collapse -->


              </div><!-- End .panel -->
              <?php } ?>



              <?php if ($logged) { ?>
              <div class="panel">


                <div class="accordion-header">


                  <div class="accordion-title"><span>Minha Conta</span></div><!-- End .accordion-title -->


                    <a class="accordion-btn opened" data-toggle="collapse" data-target="#category-list-3"></a>


                  </div><!-- End .accordion-header -->


                


                  <div id="category-list-3" class="collapse in">


                      <div class="panel-body">


                      <ul class="category-filter-list jscrollpane">


                       <li><a href="<?php echo $order; ?>"><i class="fa fa-bell"></i> <?php echo $text_order; ?></a></li>


                        <li><a href="<?php echo $download; ?>"><i class="fa fa-download"></i> <?php echo $text_download; ?></a></li>


                        <?php if ($reward) { ?>


                        <li><a href="<?php echo $reward; ?>"><i class="fa fa-plus"></i> <?php echo $text_reward; ?></a></li>


                        <?php } ?>


                        <li><a href="<?php echo $return; ?>"><i class="fa fa-reply"></i> <?php echo $text_return; ?></a></li>


                        <li><a href="<?php echo $transaction; ?>"><i class="fa fa-bank"></i> <?php echo $text_transaction; ?></a></li>


                        <li><a href="<?php echo $recurring; ?>"><i class="fa fa-briefcase"></i> <?php echo $text_recurring; ?></a></li>

                        <li><a href="index.php?route=information/confirmarpagamento"><i class="fa fa-check"></i> Confirmação de Pagamento</a></li>


                      </ul>


                  </div><!-- End .panel-body -->


                </div><!-- #collapse -->


              </div><!-- End .panel -->





              <div class="panel">


                <div class="accordion-header">


                  <div class="accordion-title"><span>Novidades</span></div><!-- End .accordion-title -->


                    <a class="accordion-btn opened" data-toggle="collapse" data-target="#category-list-4"></a>


                  </div><!-- End .accordion-header -->


                


                  <div id="category-list-4" class="collapse in">


                      <div class="panel-body">


                      <ul class="category-filter-list jscrollpane">


                         <li><a href="<?php echo $newsletter; ?>"><i class="fa fa-star"></i> <?php echo $text_newsletter; ?></a></li>


                      </ul>


                  </div><!-- End .panel-body -->


                </div><!-- #collapse -->


              </div><!-- End .panel -->

              <?php } ?>


        </div><!-- panel-group -->


      </aside>


