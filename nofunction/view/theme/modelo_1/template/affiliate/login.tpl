<?php echo $header; ?>

<div class="container">

  <div id="category-breadcrumb">

    <div class="container">

      <ul class="breadcrumb">

        <?php foreach ($breadcrumbs as $breadcrumb) { ?>

        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

        <?php } ?>

      </ul>

    </div>

  </div><!-- category-breadcrumb -->





  <?php if ($success) { ?>

  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>

  <?php } ?>

  <?php if ($error_warning) { ?>

  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>

  <?php } ?>

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

       <header class="content-title">

            <h1 class="title"><?php echo $heading_title; ?></h1>  

            <p><?php echo $text_description; ?></p>        

        </header>

      

      <div class="row">

        <div class="col-sm-6">

         

            <h2 class="checkout-title"><?php echo $text_new_affiliate; ?></h2>

            <p><?php echo $text_register_account; ?></p>

            <a class="btn btn-custom" href="<?php echo $register; ?>"><?php echo $button_continue; ?></a>

        </div>

        <div class="col-sm-6">

      

            <h2 class="checkout-title"><?php echo $text_returning_affiliate; ?></h2>

            <p><strong><?php echo $text_i_am_returning_affiliate; ?></strong></p>

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">



           <div class="input-group">

            <span class="input-group-addon"><span class="input-icon input-icon-email"></span><span class="input-text"><?php echo $entry_email; ?></span></span>

            <input type="email" required="" name="email" id="input-email" class="form-control input-lg" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>">

          </div><!-- End .input-group -->



          <div class="input-group xs-margin">

            <span class="input-group-addon"><span class="input-icon input-icon-password"></span><span class="input-text">Password*</span></span>

            <input type="password" required="" name="password" id="input-password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" class="form-control input-lg">

          </div><!-- End .input-group -->





                <a href="<?php echo $forgotten; ?>" class="btn btn-danger"><?php echo $text_forgotten; ?></a> 



              <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-custom" />

              <?php if ($redirect) { ?>

              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

              <?php } ?>

            </form>

          

        </div>

      </div>

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<?php echo $footer; ?>