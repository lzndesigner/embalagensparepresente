<?php
class ControllerModuleCodemarketAviseme extends Controller
{
    private $error = array();

    public function adicionarTabela($id_produto, $email, $dados, $id_opcao = '')
    {
        $this->db->query("INSERT INTO code_aviseme SET
            id_produto = '" . (int) $id_produto . "',
            id_opcao = '" . (!empty($id_opcao) ? $id_opcao : 0) . "',
            email = '" . $this->db->escape($email) . "',
            dados = '" . $this->db->escape(serialize($dados)) . "',
            enviado = '0',
            criado = NOW(),
            modificado = NOW()
        ");
    }

    public function enviar()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $dados = $this->request->post;
            if (isset($dados['email']) and !empty($dados['email'])) {
                $opcao    = "";
                $id_opcao = "";

                if (isset($dados['code-opcao-id'])) {
                    $id_opcao = trim($dados['code-opcao-id']);
                    $opcao    = trim($dados['code-opcao-nome']);
                } elseif (isset($dados['option'])) {
                    foreach ($dados['option'] as $o) {
                        $op = explode("@", $o);
                        $opcao .= $op[0] . " e ";
                        $id_opcao .= $op[1] . ',';
                    }

                    $id_opcao = rtrim($id_opcao, ",");
                    $opcao    = rtrim($opcao, " e ");
                }

                $email      = $dados['email'];
                $id_produto = $dados['id_produto'];

                $imagem        = "";
                $dados['link'] = $this->url->link('product/product', 'product_id=' . $id_produto);
                if (!empty($dados['imagem'])) {
                    $link   = $dados['link'];
                    $imagem = "<a href='$link'><img src=" . $dados['imagem'] . "></a><br>";
                }

                $dados = array(
                    "opcao"          => $opcao,
                    "imagem_produto" => $imagem,
                    "link"           => $dados['link'],
                    "nome_produto"   => $dados['product'],
                );

                self::adicionarTabela($id_produto, $email, $dados, $id_opcao);
                echo "Avise-me cadastrado com sucesso.";
            } else {
                echo "E-mail vazio.";
            }
        }
    }
}
