<?php
// Text
$_['text_search']              = 'Search';
$_['text_brand']               = 'Brand';
$_['text_manufacturer']        = 'Brand:';
$_['text_model']               = 'Product Code:';
$_['text_reward']              = 'Reward Points:';
$_['text_points']              = 'Price in reward points:';
$_['text_stock']               = 'Availability:';
$_['text_instock']             = 'In Stock';
$_['text_tax']                 = 'Ex Tax:';
$_['text_discount']            = ' or more ';
$_['text_option']              = 'Available Options';
$_['text_minimum']             = 'This product has a minimum quantity of %s';
$_['text_reviews']             = '%s reviews';
$_['text_write']               = 'Write a review';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews for this product.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Thank you for your review. It has been submitted to the webmaster for approval.';
$_['text_related']             = 'Related Products';
$_['text_tags']                = 'Tags:';
$_['text_error']               = 'Product not found!';
$_['text_payment_recurring']   = 'Payment Profile';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';
$_['text_day']                 = 'day';
$_['text_week']                = 'week';
$_['text_semi_month']          = 'half-month';
$_['text_month']               = 'month';
$_['text_year']                = 'year';

// Entry
$_['entry_qty']                = 'Qty';
$_['entry_name']               = 'Your Name';
$_['entry_review']             = 'Your Review';
$_['entry_rating']             = 'Rating';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';
$_['entry_captcha']            = 'Enter the code in the box below';

// Tabs
$_['tab_description']          = 'Description';
$_['tab_attribute']            = 'Specification';
$_['tab_review']               = 'Reviews (%s)';

// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';
$_['error_captcha']            = 'Warning: Verification code does not match the image!';

$_['saving_text']            = 'Save';

$_['text_whistlist']            = 'Add to <b>Wishlist</b>';
$_['text_product_esgotado']            = 'Product Sold Out';
$_['text_product_esgotado_desc']            = 'The product is not available for purchase, you can fill your data and be notified when we are in stock.';
$_['text_product_esgotado_button']            = 'Notify me';
$_['text_title_product_esgotado']            = 'Product Notification';

$_['text_your_name']            = 'Your Name';
$_['text_your_email']            = 'Your E-mail';
$_['text_your_city']            = 'City/State';
$_['text_your_phone']            = 'Phone/Mobile';
$_['text_your_product']            = 'Product';
$_['text_your_message']            = 'Message';
$_['text_button_notification']            = 'Register Notification';

$_['text_sim_frete']            = 'Freight Simulator';
$_['text_my_cep']            = 'I do not know my CEP';
$_['text_sim_parcelas']            = 'Servings simulator';
$_['text_sim_parcelas_parcelas']            = 'Plots';
$_['text_sim_parcelas_valor']            = 'Value';
$_['text_sim_parcelas_limite']            = '* Servings at least R$ 5,00';


$_['text_raing_star_1']            = 'Fair';
$_['text_raing_star_2']            = 'Normal';
$_['text_raing_star_3']            = 'Good';
$_['text_raing_star_4']            = 'Very Good';
$_['text_raing_star_5']            = 'Perfect';