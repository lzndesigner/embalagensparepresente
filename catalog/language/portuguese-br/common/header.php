<?php

// Text

$_['text_home']          = 'Principal';

$_['text_wishlist']      = 'Lista de desejos (%s)';

$_['text_shopping_cart'] = 'Carrinho de compras';

$_['text_category']      = 'Departamentos';

$_['text_account']       = 'sua conta';

$_['text_my_account']       = 'Minha conta';

$_['text_register']      = 'Cadastrar-se';

$_['text_login']         = 'Acessar';

$_['text_order']         = 'Histórico de pedidos';

$_['text_transaction']   = 'Transações';

$_['text_download']      = 'Downloads';

$_['text_logout']        = 'Sair';

$_['text_checkout']      = 'Finalizar pedido';

$_['text_search']        = 'Busca';

$_['text_all']           = 'Exibir';

$_['text_menu_contact']        = 'Contato';

$_['text_welcome']        = 'Olá, visitante. Acesse';
$_['text_welcome_or']        = 'ou';



$_['text_special']      = 'Promoção';