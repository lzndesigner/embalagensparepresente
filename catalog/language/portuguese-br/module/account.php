<?php
// Heading
$_['heading_title']    = 'Minha conta';

// Text
$_['text_register']    = 'Cadastre-se';
$_['text_login']       = 'Acessar';
$_['text_logout']      = 'Sair';
$_['text_forgotten']   = 'Solicitar nova senha';
$_['text_account']     = 'Minha conta';
$_['text_edit']        = 'Meus Dados';
$_['text_password']    = 'Modificar senha';
$_['text_address']     = 'Meus Endereços';
$_['text_wishlist']    = 'Lista de desejos';
$_['text_order']       = 'Meus Pedidos';
$_['text_download']    = 'Downloads';
$_['text_reward']      = 'Pontos';
$_['text_return']      = 'Devoluções';
$_['text_transaction'] = 'Transações';
$_['text_newsletter']  = 'Informativo por E-mail';
$_['text_recurring']   = 'Assinaturas';
$_['text_contact']   = 'Contato';
$_['text_confirm']   = 'Confirmar Pagamento';
$_['text_map']   = 'Mapa do Site';