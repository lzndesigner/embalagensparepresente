<?php
// Heading
$_['heading_title'] = 'Novidades';

// Text
$_['text_tax']      = 'Sem impostos:';
$_['text_instock']      = 'ESGOTADO';
$_['button_visit']      = 'Notifique-me';
$_['button_details']        = 'Detalhes';