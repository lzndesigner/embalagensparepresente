<?php

// Heading

$_['heading_title']        = 'Pedido cadastrado.';



// Text

$_['text_basket']          = 'Carrinho de compras';

$_['text_checkout']        = 'Finalizar pedido';

$_['text_success']         = 'Confirmação';

$_['text_customer']        = '<p>O seu pedido foi cadastrado em nossa loja.</p><p>Você pode ver o histórico dos seus pedidos através de sua <a href="%s">conta</a>, acessando o menu <a href="%s">histórico de pedidos</a>.</p><br><ul><li>Pedidos pagos através de Boleto Bancário serão enviados após a compensação do mesmo.</li><li>Pedidos pagos através de Depósito Bancário devem ter o comprovante enviado através da opção "Confirmação de Pagamento"</li></ul><p>Entre em contato com nosso <a href="%s">atendimento</a> caso tenha dúvidas.</p><p>Obrigado por comprar em nossa loja.</p>';

$_['text_guest']           = '<p>O seu pedido foi cadastrado em nossa loja.</p><p>Entre em contato com nosso <a href="%s">atendimento</a> caso tenha dúvidas.</p><p>Obrigado por comprar em nossa loja.</p>';