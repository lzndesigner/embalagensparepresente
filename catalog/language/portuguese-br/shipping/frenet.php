<?php

$_['text_title']  = 'Correios - Simulação de Envio';

$_['text_sucesso']= 'Reconexão realizada com sucesso!';

$_['text_free']   = '<span style="color:green;font-weight:bold">Grátis</span>';



$_['error_conexao']  	= 'Não foi possível estabelecer conexão com os Correios - Simulação de Envio. Tentando reconectar...';

$_['error_reconexao']  	= 'Falha na tentativa de reconectar com os Correios - Simulação de Envio. O WebService dos Correios - Simulação de Envio apresenta instabilidade ou está fora do ar.';

