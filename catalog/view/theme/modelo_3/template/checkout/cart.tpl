<?php echo $header; ?>
<div class="breadcrumb clearfix bgGrey">
  <div class="container">
    <h5><?php echo $heading_title; ?></h5>
    <ul class="">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div><!-- breadcrumb -->

<div class="container">
  <?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>


     <h1 class="title"><?php echo $heading_title; ?></h1>
     <?php if ($weight) { ?>
     <p>(<?php echo $weight; ?>)</p>
     <?php } ?>    

     <div class="row">
      <div class="col-md-8">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

          <ul class="listCartProducts">
           <?php foreach ($products as $product) { ?>
           <li>
            <?php if ($product['thumb']) { ?>
            <div class="img">
             <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
           </div><!-- img -->
           <?php } ?>
           <div class="content">
            <!-- content -->
            <h1><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h1>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-sm btn-danger" style="margin:0px 0 0 0;padding:4.5px 8px;" onclick="cart.remove('<?php echo $product['key']; ?>');"><i class="fa fa-times-circle"></i></button>

            <span class="article">
              - Ref: <?php echo $product['model']; ?>
            </span>

            <?php if (!$product['stock']) { ?>
                  <span class="text-danger" style="font-size:14px;"><i class="fa fa-exclamation-circle"></i></span>
            <?php } ?>

            <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?>
            <?php } ?>

            <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
            <?php } ?>

            <?php if ($product['recurring']) { ?>
                    <br />
                    <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
            <?php } ?>

              <div class="quantity">
                <p>Quantidade:</p>
                <input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control"/>
                 <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-sm btn-green"><i class="fa fa-refresh"></i></button>
               </div>

               <ul class="price">
                 <li class="total"><b><?php echo $product['total']; ?></b></li>
               </ul>

               

               


            <!-- content -->
          </div>
        </li>
        <?php }//foreach ?>
              <?php foreach ($vouchers as $vouchers) { ?>
               <tr>
                 <td></td>
                 <td class="text-left"><?php echo $vouchers['description']; ?></td>
                 <td class="text-left"></td>
                 <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                   <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                   <span class="input-group-btn"><button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle"></i></button></span></div></td>
                   <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                   <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                 </tr>
                 <?php } ?>
      </ul>
         </form>

<div class="clearfix"></div>
<a href="<?php echo $continue; ?>" class="btn btn-sm btn-default"><i class="fa fa-reply"></i> <?php echo $button_shopping; ?></a>

       </div><!-- col-md-8 -->

       <div class="col-md-4">
        <?php if($shipping){ ?>
        <?php echo $shipping; ?> 
        <?php } ?>

        <?php if($coupon){?>
        <?php echo $coupon; ?>
        <?php } ?>

        <?php if($voucher){ ?>
        <?php echo $voucher; ?>
        <?php } ?>

        <h4>Resumo do Pedido</h4>
        <ul class="resumeOrder">
         <?php foreach ($totals as $total) { ?>
         <li><b><?php echo $total['title']; ?>:</b>  <span class="text-left"><?php echo $total['text']; ?></span></li>
         <?php } ?>                  
       </ul>
       <hr class="clearfix">



       <div class="buttons">
         <a <?php if (!$product['stock']) { ?> disabled<?php }else{?> href="<?php echo $checkout; ?>" <?php } ?> class="btn btn-green btn-block" ><i class="fa fa-check-circle"></i> <?php echo $button_checkout; ?></a>
       </div>

     </div><!-- col-md-4 -->

   </div><!-- row -->

   <?php if ($reward) { ?>
   <header class="content-title" style="margin-top:1em;">
     <h1 class="title"><?php echo $text_next; ?></h1>
     <p><?php echo $text_next_choice; ?></p>    
   </header>     
   <div class="row">
     <div class="col-md-8 col-sm-12 col-xs-12">
       <?php echo $reward; ?>
     </div><!-- col-md-8 col-sm-12 col-xs-12 -->

     <?php } ?>
   </div><!-- row -->

   <?php echo $content_bottom; ?></div>
   <?php echo $column_right; ?></div>
 </div>
 <?php echo $footer; ?> 