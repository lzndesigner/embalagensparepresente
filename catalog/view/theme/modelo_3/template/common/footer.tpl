


<?php if ($categories) { ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/slick.css"/>
<script type="text/javascript" src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/slick.js"></script>
<!-- div -->
    <script type="text/javascript">
    $(document).ready(function(){
          $('.sliderMarcasFooter').slick({
            centerMode: true,
            arrows: false,
            dots: true,
            draggable: false,
            centerPadding: '60px',
              slidesToShow: 5,
              slidesToScroll: 5,
              autoplay: true,
              autoplaySpeed: 2000,
            });
    });
  </script>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="sliderMarcasFooter">
                <?php foreach ($categories as $category) { ?>
                <?php if ($category['manufacturer']) { ?>
                    <?php foreach ($category['manufacturer'] as $manufacturer) { ?>
                    <?php if ($manufacturer['image']){ ?>
                        <div class="item">
                            <a href="<?php echo $manufacturer['href']; ?>">
                                <!-- <h2><?php //echo $manufacturer['name']; ?></h2> -->
                                <img src="image/<?php echo $manufacturer['image']; ?>" alt="<?php echo $manufacturer['name']; ?>" class="img-responsive">
                            </a>
                        </div>
                         <?php }// if ?>
                    <?php }//foreach ?>
                <?php }//if ?>
                <?php }//foreach ?>
            </div>
        </div>
    </div>
</div>
<?php }//if ?>


<footer class="doc-footer">
    <div class="bgFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h6><?php echo $text_account; ?></h6>
                    <ul class="links">
                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                        <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                    </ul>
                    
                </div>
                <div class="col-sm-3">
                    <h6><?php echo $text_information; ?></h6>                    
                        <?php if ($informations) { ?>
                        <ul class="links">
                        <?php foreach ($informations as $information) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>
                        </ul>
                        <?php } ?>                       
                </div>
                <div class="col-sm-3">
                    <h6><?php echo $title_helpdesk; ?></h6>
                    <ul class="links">
                        <li><a href="<?php echo $marcas; ?>"><?php echo $text_manufacturer; ?></a></li>
                        <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                        <li><a href="index.php?route=information/confirmarpagamento"><?php echo $text_confirm_payment; ?></a></li>
                        <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                        <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h6><?php echo $text_contact; ?></h6>
                    <ul class="links">
                        <li><i class="fa fa-phone"></i> Telefone: <?php echo $telephone; ?></li>
                        <?php if($fax){ ?><li><i class="fa fa-whatsapp"></i> Celular: <?php echo $fax; ?></li><?php } ?>
                        <li><i class="fa fa-envelope"></i> <?php echo $email_admin; ?></li>
                        <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11&appId=1094654680570525';
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-page" data-href="https://www.facebook.com/Embalagens-para-Presente-1931525243834355/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Embalagens-para-Presente-1931525243834355/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Embalagens-para-Presente-1931525243834355/">Embalagens para Presente</a></blockquote></div>
                    </ul>
                </div>
            </div><!-- row -->
        </div><!-- container -->


<div class="bgsocial-midia">
<div class="container">
<div class="row">
    <div class="col-md-12">
        <div class="col-md-3"><h5>Encontre-nos aqui</h5></div>
        <div class="col-md-9">
                <ul class="social-icons text-left">
                      <li class="social-icons-facebook" data-toggle="tooltip" title="Facebook"><a href="https://www.facebook.com/Embalagens-para-Presente-1931525243834355/" target="_Blank"><i class="fa fa-facebook"></i></a></li>
                      <li class="social-icons-instagram" data-toggle="tooltip" title="Instagram"><a href="https://www.instagram.com/embalagensparapresente/" target="_Blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
        </div>
    </div>
</div>
</div>
</div>

        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h6>Formas de Pagamentos</h6>
                            <div class="banner_pagamentos">
                                <ul>
                                    <li class="" style="width:100%;"><img src="image/catalog/Banners/layout/banner_parcelamento_6x.png" alt="Pagamento por Depósito Bancário ou Cartões de Crédito pelo PagSeguro ou Pagar.me"></li>
                                </ul>
                            </div>
                </div><!-- col-md-8 -->
                <div class="col-md-4">
                    <h6>Formas de Envio</h6>
                         <div class="banner_pagamentos">
                                <ul>
                                    <li class=""><img src="catalog/view/images/exatos/correios.png?1" alt="Correios"></li>
                                    <li class=""><img src="catalog/view/images/exatos/pac.png?1" alt="PAC"></li>
                                    <li class=""><img src="catalog/view/images/exatos/sedex.png?1" alt="SEDEX"></li>
                                </ul>
                            </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-ms-12">
                    <div class="col-xs-12 col-sm-2">
                        <img src="_galerias/protegido_site_seguro.png" alt="Certificado de Segurança Digital" style="width:150px; margin:-14px 0 0 0;">
                    </div>
                    <div class="col-xs-12 col-sm-8 text-center">
                        <span class="copyrights">
                            <?php echo $enderecoLoja; ?>
                            <br/>
                            <?php echo $powered; ?><br/>
                        </span>
                    </div>

                    <div class="col-xs-12 col-sm-2 pull-right">
                        <b><?php echo $text_tecnologia; ?></b><br/>
                        <a href="http://www.innsystem.com.br" target="_Blank">
                            <img src="https://www.innsystem.com.br/galerias/logo_preto_azul.png" alt="InnSystem - Inovação em Sistemas" style="width:110px; margin:0px 0 0 0;">
                        </a>
                    </div>
                </div><!-- col-sm-12 -->        
            </div>
        </div>
    </div>

</footer>

<script type="text/javascript">
    function subscribe(){
    var formNews = $('form[name="formNewsletter"]');
    var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var nome = $('#txtfirstname').val();
    var email = $('#txtemail').val();
    if(nome != "" || email != "")
    {
        if(!emailpattern.test(email))
        {
            $( "#msgRetorno" ).append("<p>Insira um E-mail Válido</p>");
            return false;
        }
        else
        {
            $.ajax({
                url: 'index.php?route=module/newsletters/news',
                type: 'post',
                data: {
                    'nome': nome,
                    'email': email
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#btnNewsletter').html('Aguarde').attr('disabled', true);
                },                
                success: function(json) {
                    if (json.message === 'existe') {
                        $( "#msgRetorno" ).append("<div class='alert alert-danger'>E-mail já Cadastrado, utilize outro e-mail válido</div>");
                           $('#btnNewsletter').html('<i class="fa fa-check"></i> Registrar').attr('disabled', false);
                        setTimeout(function(){
                           $( "#msgRetorno" ).empty();      
                        }, 5000);
                    } else if(json.message === 'falha'){
                        $( "#msgRetorno" ).append("<div class='alert alert-danger'>Não foi possível cadastrar.</div>");
                           $('#btnNewsletter').html('<i class="fa fa-check"></i> Registrar').attr('disabled', false);
                        setTimeout(function(){
                           $( "#msgRetorno" ).empty();      
                        }, 5000);
                    } else if(json.message === 'ok'){
                        $( "#msgRetorno" ).append("<div class='alert alert-success'>Você receberá por e-mail, nossas promoções e novidades.<br/><small>* Caso deseja cancelar o <b>newsletter</b> basta responder o e-mail com o assunto <b>SAIR</b></small></div>");
                        setTimeout(function(){
                           $('#btnNewsletter').html('<i class="fa fa-check"></i> Registrar').attr('disabled', false);
                           $( "#msgRetorno" ).empty();      
                        }, 10000);
                    }                    
                },
                afterSend: function(){
                    $('#btnNewsletter').html('<i class="fa fa-check"></i> Registrar').attr('disabled', false);
                     setTimeout(function(){
                       $( "#msgRetorno" ).empty();      
                    }, 5000);
                }

            });
            return false;
        }
    }
    else
    {
         
        $('#btnNewsletter').html('Aguarde').attr('disabled', true);
        $( "#msgRetorno" ).append("<div class='alert alert-warning'>E-mail é necessário</div>");      
            $('#btnNewsletter').html('<i class="fa fa-check"></i> Registrar').attr('disabled', false);
        setTimeout(function(){
           $( "#msgRetorno" ).empty();      
        }, 5000);              
        $(email).focus();
        return false;
    }
}
</script>
<script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/bootstrap.min.js"></script>
<script type='text/javascript' src='catalog/view/javascript/jquery.cookie.js'></script>
<script type='text/javascript'>
$(function(){

    if($.cookie('box-pop-up') == null){   
        $(document).keyup(function(e) {
        if (e.keyCode == 27) $('#box-popup-ctn').fadeOut();  // esc
        });

        $("#box-popup").click(function(e) { 
            e.stopPropagation();
        });

        $("#box-popup-ctn").click(function(e) { 
            $('#box-popup-ctn').fadeOut();
        });

        $("#buttonClosePopup").click(function(e) { 
            $('#box-popup-ctn').fadeOut();
        });

        $('#box-popup-ctn').show();
    }

        var date = new Date();
        date.setTime(date.getTime() + (30 * 60 * 1000));
        $.cookie('box-pop-up', 'box-pop', { expires: date });
});
</script>
</div><!-- End .bodyWrap -->
</body></html>