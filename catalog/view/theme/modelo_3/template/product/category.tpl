<?php echo $header; ?>
<div class="breadcrumb clearfix bgGrey">
  <div class="container">
    <h5><?php echo $heading_title; ?></h5>
    <ul class="">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div><!-- breadcrumb -->

<section class="">
  <div class="container">
    <div class="row">
      <?php echo $column_left; ?>

      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>

      <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
       <div class="manageResults category" style="margin-bottom:20px; text-align:left;">
        <?php if ($categories) { ?>
        <div class="subCategorys">
          <h4>Sub-Departamentos</h4>
          <ul>
          <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
          <?php foreach ($categories as $category) { ?>
          <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul></div>
        <?php } ?>


        <header class="head familyOs"><h2><?php echo $heading_title; ?></h2></header>
        <?php if ($thumb) { ?>      
        <img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" class="img-responsive" />
        <?php } ?>
        <?php  if($description == "<p><br></p>") {}else{ ?>
        <?php if ($description) { ?>      
        <span class="resultsCount"><?php echo $description; ?></span>                            
        <?php } ?>
        <?php } ?>

        <?php if ($products) { ?>
        <div class="row hide">
        <div class="col-xs-12 col-sm-12 col-md-4">
          <a href="<?php echo $compare; ?>" class="btn btn-default pull-left btn-sm" style="margin:50px 0;" id="compare-total"><?php echo $text_compare; ?></a>
        </div>
        <div class="col-xs-6 col-sm-6 col-sm-4">
          <h6><?php echo $text_sort; ?></h6>
          <select id="input-sort" class="light" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>                                                
        </div>
        <div class="col-xs-6 col-sm-6 col-sm-4">
          <h6><?php echo $text_limit; ?></h6>
          <select id="input-limit" class="light" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        </div>

        <ul class="change-prod-style" style="display:none;">
          <li><a class="prod-style-2" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid-s.png" alt="Grid"></a></li>
          <li class="active"><a class="prod-style-3" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid-small.png" alt="Grid small"></a></li>
          <li><a class="prod-style-4" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid-4.png" alt="Grid"></a></li>
          <li><a class="prod-style-5" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid.png" alt="Grid"></a></li>
        </ul>
      </div>

      <ul class="products categorys item-back-display style3 col-grid-4 space-30 clearfix">

        <?php foreach ($products as $product) { ?>
                <li class="product text-center">
                  <div class="productImages">
                    <div class="image-default"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
                  </div>
                  <div class="productInfo">
                    <h3><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>

        <?php if ($product['price']) { ?>
        <p><small><b>A partir de:</b></small></p>
        <ul class="pricestyle">
          <?php if (!$product['special']) { ?>
          <li><span class="priceReal"><?php echo $product['price']; ?></span></li>
          <?php } else { ?>
          <li>De: <strike><?php echo $product['price']; ?></strike></li>
          <li>Por: <span class="priceReal"><?php echo $product['special']; ?></span></li>
          <?php } ?> 
        </ul>
        <?php } ?>
        
                    <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:80%; margin:.5em auto;' />";}?>
                    <?php if (!$product['quantity'] <= '0') { ?>
        <div class="btn-group btn-group-justified" role="group">

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-purple" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></button>
          </div>

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-default" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_details; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';"><i class="fa fa-shopping-cart"></i> <?php echo $button_details; ?></button>
          </div>

        </div>
                    <?php }else{ ?>

                    <span class="alert alert-danger alert-nostock"><?php echo $text_instock; ?></span>
                    
                    <div class="button-group">
                      <button type="submit" class="btn btn-purple" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_visit; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';" style="width:100%; margin:0 10%;"><i class="fa fa-shopping-cart"></i> <?php echo $button_visit; ?></button>
                    </div>

                    <?php } ?>

                  </div>
                </li>
        <?php } ?>





      </ul>
<div class="clearfix"></div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>

      <?php } ?>



      <?php if (!$categories && !$products) { ?>
      <div class="alert alert-info">
        <p><?php echo $text_empty; ?></p>  
      </div>      
      <div class="buttons">
        <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?>
    </div><!-- #content -->
    <?php echo $column_right; ?>
  </div><!-- row -->
</div><!-- container -->
</section><!--//wrap-->


<?php echo $footer; ?>



